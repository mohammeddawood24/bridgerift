$(document).ready(function() {
	// var page_id = $("#page_id").val();
	/*var visibleColumns = [
		{width: '20px', targets: 0},
		{width: '20px', targets: 1},
		{width: '20px', targets: 2},
		{width: '20px', targets: 3},
		{width: '20px', targets: 4},
		{width: '20px', targets: 5},
		{width: '20px', targets: 6},
		{width: '20px', targets: 7}
	];
	var dataColumns = [
		{data: "row_id"},
		{data: "lead_name"},
		{data: "company"},
		{data: "email"},
		{data: "phone"},
		{data: "lead_source"},
		{data: "lead_owner"},
		{data: "action"}
	];*/
	/*$('#leads').DataTable({
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        deferLoading: 0,
        order: [], //Initial no order.
        ordering: true,
        pageLength: 20,
        lengthMenu: [10, 20, 50, 100],
        autoWidth: false,
        columnDefs: visibleColumns,
        // scrollY: height + "px",
        scrollX: true,
        scrollCollapse: true,
        ajax: {
            url: base_url+'admin_home/manage_leads/search/'+page_id,
            type: "POST"
        },
        oLanguage: {sProcessing: "<div class='loading'>Loading&#8230;</div>"},
        columns: dataColumns,
        initComplete: function () {
        	$("#leads_filter").append('<a href="javascript:void(0)" class="btn btn-sm btn-primary" data-url-load="" data-toggle="modal" data-target="#edit-lead" style="margin-left: 10px;"><i class="fa fa-plus"></i> Add</a>');
        }
    });*/

    $('#leads').DataTable({});
	$(".dataTables_wrapper").addClass("mx-0");

	$('#edit-task').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		$(this).find('.modal-content').load(loadurl, function(){
			$("#time-row").hide();
		});
	});

	$('#edit-task').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
	});

	$("body").on("change", "#activity_type", function(){
		var acti = $("#activity_type").val();
		if (acti == 0) {
			$("#time-row").fadeOut();
		}else {
			$("#time-row").fadeIn();
		}
	});

	$("body").on("change", "#company_id", function(){
		var company_id = $("#company_id").val();
		$.ajax({
			'type': 'POST',
			'url': base_url+'admin_home/company/get_company_contacts/'+company_id,
			'dataType': 'text',
			'cache': false,
			'success': function(d){
				$("body #contact_id").html(d);
			}
		});
	});
});