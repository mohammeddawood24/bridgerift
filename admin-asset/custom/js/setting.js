$(function () {
   $('#settings').on('show.bs.modal', function (e) {
      var loadurl = e.relatedTarget.dataset.loadUrl;
      $(this).find('.modal-content').load(loadurl, function() {
         initModal();
      });
   });
   
   $('#settings').on('hidden.bs.modal', function (e) {
      $('.modal-content').html('');
      tinyMCE.editors = [];
   });
});

function initModal(){
   tinymce.init({
      selector:'textarea[name="about"], textarea[name="tc"]',
      menubar: false,
      height: 200
   });
}