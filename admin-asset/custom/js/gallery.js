$(document).ready(function() {
	$('#gallery').DataTable({});
	$("#gallery_filter").append('<a href="#" data-toggle="modal" data-target="#edit-gallery" style="margin-left:20px;" class="btn btn-sm btn-outline-primary waves-effect waves-light"><i class="icon-plus"></i> Add</a>');
	$(".dataTables_wrapper").addClass("mx-0");
});