$(document).ready(function() {
	$('#testi').DataTable({});
	$(".dataTables_wrapper").addClass("mx-0");

	$('#edit-testi').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		$(this).find('.modal-content').load(loadurl, function(){});
	});

	$('#edit-testi').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
	});
});