$(document).ready(function() {
	$('#service-faq').DataTable({});
	$('#service-testimonials').DataTable({});
	$('#service-pricing').DataTable({});
	
	$(".dataTables_wrapper").addClass("mx-0");

	$('#edit-service').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		var id = e.relatedTarget.dataset.id;
		if (id == 4) {
			$(this).find(".modal-dialog").addClass('modal-xlg');
		}else {
			$(this).find(".modal-dialog").removeClass('modal-xlg');
		}

		$(this).find('.modal-content').load(loadurl, function() {
			initModal();
		});
	});
   
	$('#edit-service').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
		tinyMCE.editors = [];
	});
});

function initModal(){
	tinymce.init({
		selector:'textarea[name="about"]',
		menubar: false,
		height: 200
	});

	$("#skills").tagsinput();
}