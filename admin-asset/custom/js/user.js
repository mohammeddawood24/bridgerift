$(document).ready(function() {
	$('#user').DataTable({});
	$(".dataTables_wrapper").addClass("mx-0");

	$('#edit-user').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		$(this).find('.modal-content').load(loadurl, function(){});
	});

	$('#edit-user').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
	});
});