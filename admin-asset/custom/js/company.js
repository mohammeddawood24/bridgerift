$(document).ready(function() {
	$('#company').DataTable({
		initComplete: function () {
            $('<a id="add" href="javascript:void(0);" data-load-url="' + base_url + 'admin_home/company/view/" class="btn" style="padding: 2px 5px;" data-toggle="modal" data-target="#edit-company-popup"><i class="icon-plus"></i> Add</a>').appendTo('div#company_filter');
        }
	});
	$(".dataTables_wrapper").addClass("mx-0");

	$('#edit-company-popup').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		$(this).find('.modal-content').load(loadurl, function(){});
	});

	$('#edit-company-popup').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
	});

	$('#contact').DataTable({
		initComplete: function () {
			var company_id = $("#company_id").val();
            $('<a id="add" href="javascript:void(0);" data-load-url="' + base_url + 'admin_home/company/contact_view/'+company_id+'/" class="btn" style="padding: 2px 5px;" data-toggle="modal" data-target="#edit-contact-popup"><i class="icon-plus"></i> Add</a>').appendTo('div#contact_filter');
        }
	});
	
	$('#edit-contact-popup').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		$(this).find('.modal-content').load(loadurl, function(){});
	});

	$('#edit-contact-popup').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
	});
});