$(document).ready(function() {
	$('#edit-logo').on('show.bs.modal', function (e) {
		var loadurl = e.relatedTarget.dataset.loadUrl;
		$(this).find('.modal-content').load(loadurl, function(){});
	});

	$('#edit-logo').on('hidden.bs.modal', function (e) {
		$('.modal-content').html('');
	});
});