<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Manage_user extends MX_Controller{
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
		if ($this->data['user']['role'] != 0) {
			# code...
			redirect('admin_home');
		}
		$this->data['pg'] = 'user';
		$this->load->model('user_m');
	}

	private function hash_password($pwd = 'BridgeRift@)!@)'){
		$options = array('cost'=>'12');
		return password_hash($pwd, PASSWORD_BCRYPT, $options);
	}

	function index() {
		$this->data['title'] = 'Manage Active Users';
		
		$this->data['users'] = $this->user_m->get_all();

		$this->load->view('index', $this->data);
	}

	function view($id = NULL) {
		$this->data['user_id'] = $id;
		$this->data['user'] = '';
		if ($id) {
			# code...
			$this->data['user'] = $this->user_m->get($id);
		}

		$this->load->view('edit', $this->data);
	}

	function edit(){
		$user_id = $this->input->post('user_id');

		$data = array('name'=>$this->input->post('name'), 'email'=>$this->input->post('email'),
					'status'=>$this->input->post('status'), 'role'=>$this->input->post('role'));

		if ($user_id) {
			# code...
			$data['updated'] = date('Y-m-d');
			$this->user_m->update($user_id, $data);
		}else {
			$data['password'] = $this->hash_password();
			$data['created_on'] = date('Y-m-d');
			$this->user_m->insert($data);
		}

		redirect('manage_user');
	}
}