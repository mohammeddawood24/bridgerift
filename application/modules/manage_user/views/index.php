<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6 text-right">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-user" data-load-url="<?=base_url()?>manage_user/view"><i class="fa fa-plus"></i> Add</button>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-md-12">
                <div class="card card-body table-responsive">
                    <table id="user" class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Password</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                                foreach ($users as $user) {
                                    # code...
                                    $action = '<a href="#" data-toggle="modal" data-target="#edit-user" data-load-url="'.base_url().'manage_user/view/'.$user->id.'"><i class="icon-pencil"></i></a>';
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td><?=$user->name;?></td>
                                <td><?=$user->email;?></td>
                                <td><?=getUserRoles($user->role);?></td>
                                <td><?=$user->decrypt_pwd;?></td>
                                <td><?=getUserStatus($user->status);?></td>
                                <td><?=$action;?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-user">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>