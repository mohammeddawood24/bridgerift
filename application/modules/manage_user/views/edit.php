<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0"><?=($user)?'Edit':'Add';?> User</h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('manage_user/edit');?>
<input type="hidden" name="user_id" value="<?=$user_id;?>">

<div class="modal-body">
    <div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Full Name <span class="text-danger">*</span></label>
	            <input type="text" name="name" class="form-control" required value="<?=($user)?$user->name:'';?>">
	        </div>
	    </div>
	</div>
	<div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Email <span class="text-danger">*</span></label>
	            <input type="email" name="email" class="form-control" required value="<?=($user)?$user->email:'';?>">
	        </div>
	    </div>
	</div>
	<div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Role <span class="text-danger">*</span></label>
	            <?=form_dropdown('role', getUserRoles(), ($user?$user->role:''), 'class="form-control" required');?>
	        </div>
	    </div>
	</div>
	<div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Status <span class="text-danger">*</span></label>
	            <?=form_dropdown('status', getUserStatus(), ($user?$user->status:''), 'class="form-control" required');?>
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>