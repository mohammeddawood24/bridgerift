<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>BridgeRift | Home</title>
	<meta charset="UTF-8">
	<link rel="shortcut icon" href="#"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
	<!-- Font-Awesome -->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/fontawesome-all.css">
	<!-- Slider -->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/slider.css">
	<!--Navigation Menu CSS-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/navigation.css">
	<!-- Owl carousel Styles -->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/owl.theme.default.css">
	<!--Animate CSS-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/animate.css">
	<!-- Main Styles -->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/styles.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/custom/style.css">
</head>

<body>
<!-- Preloader Start-->
<div id="preloader">
	<div class="row loader">
		<div class="loader-icon"></div>
	</div>
</div>

<?=modules::run('common/website_menu');?>
<main>