<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Menu</li>
                <li>
                    <?=anchor('admin_home', '<i class="icon-accelerator"></i> <span> Dashboard </span>');?>
                </li>
                <li>
                    <?php $leadMenu = ($user['role'] == 0 || $user['role'] == 1)?'Manage Leads':'My Leads';
                        echo anchor('manage_leads', '<i class="icon-todolist"></i> <span> '.$leadMenu.' </span>');?>
                </li>
                <?php if($user['role'] == 0){ ?>
                <li>
                    <?=anchor('admin_home/company', '<i class="icon-squares"></i> <span> Manage Companies</span>');?>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="icon-squares"></i> <span> Manage Website <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span>
                    </a>
                    <ul class="submenu">
                        <li><?=anchor('admin_home/services', 'Manage Services');?></li>
                        <li><?=anchor('admin_home/gallery', 'Manage Gallery');?></li>
                        <li><?=anchor('admin_home/settings', 'Site Settings');?></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="icon-profile"></i> <span> Manage Users <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span>
                    </a>
                    <ul class="submenu">
                        <li><?=anchor('manage_user', 'Active Users');?></li>
                        <li><?=anchor('manage_user/inactive', 'In-active Users');?></li>
                    </ul>
                </li>
                <?php } ?>
                <li>
                    <?=anchor('admin/logout', '<i class="ti-power-off"></i> <span> Logout</span>');?>
                </li>
            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>