<header id="nav-transparent">
	<div>
		<nav id="navigation4" class="container navigation">
			<div class="nav-header">
				<a class="nav-brand" href="index.html">
					<img src="<?=base_url();?>assets/img/logo.png" alt="logo" class="main-logo" id="light_logo" style="width: 60%;">
					<img src="<?=base_url();?>assets/img/logo-inversed.png" alt="logo" class="main-logo" id="main_logo" style="width: 60%;">
				</a>
				<div class="nav-toggle"></div>
			</div>
			<div class="nav-menus-wrapper">
				<ul class="nav-menu align-to-right">
					<li><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Services</a>
						<ul class="nav-dropdown">
							<li><a href="#">Sales-Mantra</a></li>
							<li><a href="#">Business-Edge Coaching</a></li>
							<li><a href="#">Boot-Camp for Professionals</a></li>
						</ul>
					</li>
					<li><a href="#">Contact</a></li>
					<li><a href="#" style="background: #e31111;color: #fff;">Schedule Demo</a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>