<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-4 foot-col-1">
				<img src="<?=base_url();?>assets/img/logo.png" alt="logo" class="img-fluid" style="width: 70%;">
				<p class="s-mrt-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt
				ut labore et dolore magna aliqua.</p>
				<ul class="social-footer">
					<li>
						<a href="#"> <i class="fab fa-twitter iconhover"></i>
						</a>
					</li>
					<li>
						<a href="#"> <i class="fab fa-linkedin-in iconhover"></i>
						</a>
					</li>
					<li>
						<a href="#"> <i class="fab fa-facebook-f iconhover"></i>
						</a>
					</li>
					<li>
						<a href="#"> <i class="fab fa-youtube iconhover"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 offset-lg-1 foot-col-2 footer-col-clone">
				<h3>OUR SERVICES</h3>
				<ul>
					<li><a href="#">Sales-Mantra</a>
					</li>
					<li><a href="#">Business-Edge Coaching</a>
					</li>
					<li><a href="#">Boot-Camp for Professionals</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 offset-lg-1 foot-col-3 footer-col-clone">
				<h3>Useful Links</h3>
				<ul>
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">About</a>
					</li>
					<li><a href="#">Contact</a>
					</li>
					<li><a href="#">Blog</a>
					</li>
					<li><a href="#">Schedule Demo</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-12 foot-bottom">
				<p>Copyrights &copy; <?=date('Y');?> BridgeRift. All Rights Reserved.</p>
			</div>
		</div>
	</div>
</footer>
</main>
<script src="<?=base_url();?>assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Navbar JS -->
<script src="<?=base_url();?>assets/js/navigation.js"></script>
<script src="<?=base_url();?>assets/js/navigation.fixed.js"></script>
<!-- Swiper Slider -->
<script src="<?=base_url();?>assets/js/swiper.min.js"></script>
<!-- Popper -->
<script src="<?=base_url();?>assets/js/popper.min.js"></script>
<!-- Include js plugin -->
<script src="<?=base_url();?>assets/js/owl.carousel.min.js"></script>
<!-- Main JS -->
<script src="<?=base_url();?>assets/js/main.js"></script>
</body>
</html>