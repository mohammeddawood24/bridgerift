			<!-- content end -->
			<footer class="footer">
			    Copyrights &copy; <?=date('Y');?> <a href="">BridgeRift</a>. All Rights Reserved.
			</footer>
		</div>
	</div>
	<!-- jQuery  -->
    <script src="<?=base_url();?>admin-asset/js/jquery.min.js"></script>
    <script src="<?=base_url();?>admin-asset/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url();?>admin-asset/js/metismenu.min.js"></script>
    <script src="<?=base_url();?>admin-asset/js/jquery.slimscroll.js"></script>
    <script src="<?=base_url();?>admin-asset/js/waves.min.js"></script>

    <script src="<?=base_url();?>admin-asset/plugins/tinymce/tinymce.min.js"></script>

    <!-- datatable js -->
    <script src="<?=base_url();?>admin-asset/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url();?>admin-asset/plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- App js -->
    <script src="<?=base_url();?>admin-asset/js/app.js"></script>

    <!-- tags -->
    <script src="<?=base_url();?>admin-asset/js/bootstrap-tagsinput.min.js"></script>

    <!-- Custom JS -->
    <?php if(isset($pg)){?>
        <script src="<?=base_url();?>admin-asset/custom/js/<?=$pg;?>.js"></script>
    <?php }?>

</body>

</html>