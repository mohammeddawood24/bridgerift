<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>BridgeRift | Admin</title>
    <meta content="" name="description" />
    <meta content="Mohammed Dawood" name="author" />
    <link rel="shortcut icon" href="<?=base_url();?>admin-asset/images/favicon.ico">

    <!-- DataTables -->
    <link href="<?=base_url();?>admin-asset/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="<?=base_url();?>admin-asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>admin-asset/css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>admin-asset/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>admin-asset/css/style.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>admin-asset/custom/style.css" rel="stylesheet" type="text/css">

    <!-- tags -->
    <link href="<?=base_url();?>admin-asset/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        var base_url = '<?=base_url();?>';
    </script>
</head>

<body class="fixed-left widescreen">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left px-3">
                <a href="<?=base_url();?>admin" class="logo">
                    <span class="logo-light">
                        <img src="<?=base_url();?>admin-asset/images/logo-white.png" class="img-fluid">
                    </span>
                    <span class="logo-sm">
                        <i class="mdi mdi-camera-control"></i>
                    </span>
                </a>
            </div>

            <nav class="navbar-custom">
                <ul class="navbar-right list-inline float-right mb-0">
                    <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                        <?=anchor(base_url(), '<i class="fa fa-globe"></i> &nbsp; Visit Website', 'class="btn btn-primary btn-sm" target="_blank"');?>
                    </li>
                    <!-- full screen -->
                    <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                        <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                            <i class="mdi mdi-arrow-expand-all noti-icon"></i>
                        </a>
                    </li>

                    <li class="dropdown notification-list list-inline-item">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="<?=base_url();?>admin-asset/images/users/user-4.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle"></i> Profile</a>
                                <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline"></i> Change Password</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a>
                            </div>
                        </div>
                    </li>

                </ul>

                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                    <li class="d-none d-md-inline-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                </ul>

            </nav>

        </div>

        <!-- Menu -->
        <?=modules::run('common/menu');?>
        <div class="content-page">