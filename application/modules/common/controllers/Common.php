<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Common extends MX_Controller {
	function __construct() {
		parent::__construct();
	}

	function header() {
		$this->load->view('header');
	}

	function menu() {
		$this->load->view('menu');
	}
	
	function footer() {
		$this->load->view('footer');
	}

	function website_header() {
		$this->load->view('website_header');
	}

	function website_menu() {
		$this->load->view('website_menu');
	}
	
	function website_footer() {
		$this->load->view('website_footer');
	}
}