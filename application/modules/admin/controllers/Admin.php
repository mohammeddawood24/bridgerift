<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user(false);
		$this->data['pg'] = 'user';
		$this->load->model('user_m');
		$this->load->helper('string');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this; 
	}

	private function hash_password($pwd = 'BridgeRift@)!@)'){
		$options = array('cost'=>'12');
		return password_hash($pwd, PASSWORD_BCRYPT, $options);
	}

	function index(){
	    require_user();
	    redirect('admin_home');
	}

	function login() {
		$this->form_validation->set_rules('email', 'Email is mandatory', 'trim|required|xss_clean|valid_email|callback__call_login');
		$this->form_validation->set_rules('password', 'Password is mandatory', 'trim|required|xss_clean');
		$this->form_validation->set_message('_call_login', 'User Email and Password does not match. Please try again.');

		if ($this->form_validation->run($this)) {
			# code...
			$redirect = $this->session->userdata('redirect');
			if (!empty($redirect)) {
		        $this->session->unset_userdata(array('redirect' => ''));
		        redirect($redirect);
			}
			$user =  require_user();
			redirect('admin_home');
		}
		$this->load->view('login/index', $this->data);
	}

	function _call_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$where = array('email'=>$email, 'status'=>1);
		$user = $this->user_m->get_by($where);

		if ($user) {
			# code...
			if (password_verify($password, $user->password)) {
				$userdata = array(
			        'id' => $user->id,
			        'name' => $user->name,
			        'email' => $user->email,
			        'role'=>$user->role,
			        'logged_in' => TRUE,
				);
				$this->session->set_userdata('bridge_user', $userdata);

				return TRUE;
			}else {
				return FALSE;
			}
		}else {
			return FALSE;
		}
	}

	function logout() {
	    $this->session->sess_destroy();
	    redirect('admin/login');
	}
}