<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>BridgeRift | Login</title>
        <meta content="" name="description" />
        <meta content="Mohammed Dawood" name="author"/>
        <link rel="shortcut icon" href="<?=base_url();?>admin-asset/images/favicon.ico">

        <link href="<?=base_url();?>admin-asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url();?>admin-asset/css/metismenu.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url();?>admin-asset/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url();?>admin-asset/css/style.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            .login-bg{
                background: url(<?=base_url();?>admin-asset/images/login-bg.jpg);
                background-size: cover;
            }
            .wrapper-page{
                margin: 7.5%;
            }
        </style>
    </head>

    <body>

        <!-- Begin page -->
        <div class="accountbg login-bg"></div>
        <div class="wrapper-page float-right">
            <div class="card card-pages shadow-none">

                <div class="card-body">
                    <div class="text-center m-t-0 m-b-15">
                        <a href="" class="logo logo-admin">
                            <img src="<?=base_url();?>admin-asset/images/logo.png" alt="BridgeRift Logo" class="img-fluid" width="250">
                        </a>
                    </div>
                    <h5 class="font-18 text-center">Sign in</h5>
                     <?php if(validation_errors()) echo "<div id='msg' class='alert alert-warning text-center'>".validation_errors()."</div>"?>
                    <?=form_open('admin/login', 'class="form-horizontal m-t-30"');?>
                        <div class="form-group">
                            <div class="col-12">
                                <label>Email</label>
                                <input class="form-control" type="email" required placeholder="Email" name="email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <label>Password</label>
                                <input class="form-control" type="password" required="" placeholder="Password" name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <div class="checkbox checkbox-primary">
                                    <div class="custom-control custom-checkbox" style="display: inline-block;">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1"> Remember me</label>
                                    </div>
                                    <a href="" class="text-muted float-right"><i class="fa fa-lock m-r-5"></i> Forgot password?</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center m-t-20">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?=base_url();?>admin-asset/js/jquery.min.js"></script>
        <script src="<?=base_url();?>admin-asset/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url();?>admin-asset/js/metismenu.min.js"></script>
        <script src="<?=base_url();?>admin-asset/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url();?>admin-asset/js/waves.min.js"></script>

        <!-- App js -->
        <script src="<?=base_url();?>admin-asset/js/app.js"></script>
        
    </body>

</html>