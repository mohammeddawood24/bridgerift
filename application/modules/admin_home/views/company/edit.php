<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0"><?=($content)?'Edit':'Add';?> Company <span class="text-danger">*</span></h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/company/edit');?>
<input type="hidden" name="company_id" value="<?=$company_id;?>">
<div class="modal-body">
    <div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Company Name <span class="text-danger">*</span></label>
	            <input type="text" name="name" class="form-control" required value="<?=($content)?$content->name:'';?>" placeholder="Enter company name">
	        </div>
	    </div>
	</div>
    <div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Website Link <span class="text-danger">*</span></label>
	            <input type="text" name="website" class="form-control" required value="<?=($content)?$content->website:'';?>" placeholder="Ex: http(s)://www.example.com">
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>