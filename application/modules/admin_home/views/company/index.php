<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <div class="table-responsive">
                        <table id="company" class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Company Name</th>
                                    <th>Website Link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0;
                                    foreach($content as $company){
                                        $action = '<a href="javascript:void(0)" data-load-url="'.base_url().'admin_home/company/view/'.$company->id.'" data-toggle="modal" data-target="#edit-company-popup"><i class="icon-pencil"></i></a>&nbsp;&nbsp;'.
                                            anchor('admin_home/company/contacts/'.$company->id, '<i class="fas fa-user"></i>');
                                ?>
                                <tr>
                                    <td><?=++$i;?></td>
                                    <td><?=$company->name;?></td>
                                    <td><?=$company->website?></td>
                                    <td><?=$action;?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-company-popup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>