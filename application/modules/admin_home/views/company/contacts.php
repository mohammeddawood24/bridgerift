<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <div class="table-responsive">
                        <input type="hidden" name="company_id" id="company_id" value="<?=$company_id;?>">
                        <table id="contact" class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Contact Person</th>
                                    <th>Primary Mobile no.</th>
                                    <th>Email ID</th>
                                    <th>Secondary Mobile no.</th>
                                    <th>Skype ID</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0;
                                    foreach($content as $user){
                                        $action = '<a href="javascript:void(0)" data-load-url="'.base_url().'admin_home/company/contact_view/'.$company_id.'/'.$user->id.'" data-toggle="modal" data-target="#edit-contact-popup"><i class="icon-pencil"></i></a>';
                                ?>
                                <tr>
                                    <td><?=++$i;?></td>
                                    <td><?=$user->name;?></td>
                                    <td><?=$user->mobile;?></td>
                                    <td><?=$user->email;?></td>
                                    <td><?=$user->mobile1;?></td>
                                    <td><?=$user->skype;?></td>
                                    <td><?=$action;?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-contact-popup">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>