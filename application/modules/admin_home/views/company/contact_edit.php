<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0"><?=($content)?'Edit':'Add';?> Contact <span class="text-danger">*</span></h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/company/contact_edit');?>
<input type="hidden" name="company_id" value="<?=$company_id;?>">
<input type="hidden" name="contact_id" value="<?=$contact_id;?>">
<div class="modal-body">
    <div class="row">
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Contact Name <span class="text-danger">*</span></label>
	            <input type="text" name="name" class="form-control" required value="<?=($content)?$content->name:'';?>" placeholder="Enter contact name">
	        </div>
	    </div>
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Email <span class="text-danger">*</span></label>
	            <input type="email" name="email" class="form-control" required value="<?=($content)?$content->email:'';?>" placeholder="Enter email id">
	        </div>
	    </div>
	</div>
    <div class="row">
	    <div class="col-4">
	    	<div class="form-group">
	    		<label>Primary Mobile No <span class="text-danger">*</span></label>
	            <input type="number" name="mobile" class="form-control" required value="<?=($content)?$content->mobile:'';?>">
	        </div>
	    </div>
	    <div class="col-4">
	    	<div class="form-group">
	    		<label>Secondary Mobile No</label>
	            <input type="number" name="mobile1" class="form-control" value="<?=($content)?$content->mobile1:'';?>">
	        </div>
	    </div>
	    <div class="col-4">
	    	<div class="form-group">
	    		<label>Skype ID</label>
	            <input type="text" name="skype" class="form-control" value="<?=($content)?$content->skype:'';?>">
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>