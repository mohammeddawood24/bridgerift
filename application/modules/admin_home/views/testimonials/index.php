<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="#" data-load-url="<?=base_url();?>admin_home/testi_view" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-testi"><i class="fa fa-plus"></i> &nbsp; Add</a>
                    <?=anchor('admin_home/settings', '<i class="fa fa-reply"></i> &nbsp; Back', 'class="btn btn-sm btn-danger"');?>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-md-12">
                <div class="card card-body table-responsive">
                    <table id="testi" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="20%">User Details</th>
                                <th width="40%">Message</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($testimonials){
                                    $i=0;
                                    foreach($testimonials as $key => $val){
                                        $action = '<a href="javascript:void(0)" data-load-url="'.base_url().'admin_home/testi_view/'.$val->id.'" data-toggle="modal" data-target="#edit-testi"><i class="icon-pencil text-primary"></i></a> &nbsp;'.
                                                anchor('admin_home/testi_delete/'.$val->id, '<i class="icon-trash-bin text-danger"></i>', 'title="Delete"');
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td>
                                    <?=$val->username;?><br>
                                    <span class="text-muted"><?=$val->designation;?></span>
                                </td>
                                <td><?=$val->message;?></td>
                                <td class="text-center"><?=$action;?></td>
                            </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-testi">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>