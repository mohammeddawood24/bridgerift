<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0"><?=($testimonial)?'Edit':'Add';?> Testimonial</h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/edit');?>
<input type="hidden" name="testi_id" value="<?=$testi_id;?>">

<div class="modal-body">
    <div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Username <span class="text-danger">*</span></label>
	            <input type="text" name="username" class="form-control" required value="<?=($testimonial)?$testimonial->username:'';?>">
	        </div>
	    </div>
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Designation <span class="text-danger">*</span></label>
	            <input type="text" name="designation" class="form-control" required value="<?=($testimonial)?$testimonial->designation:'';?>">
	        </div>
	    </div>
	</div>
	<div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	    		<label>Message <span class="text-danger">*</span></label>
	            <textarea class="form-control" name="msg" rows="4" required><?=($testimonial)?$testimonial->message:'';?></textarea>
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>