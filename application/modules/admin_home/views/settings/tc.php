<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0">Terms & Conditions <span class="text-danger">*</span></h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/update_settings');?>
<input type="hidden" name="page_id" value="<?=$page_id;?>">
<div class="modal-body">
    <div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	            <textarea name="tc" rows="12"><?=($content)?$content->terms_and_conditions:'';?></textarea>
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>