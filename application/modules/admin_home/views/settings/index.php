<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6">
                    
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-md-3">
                <a href="#" data-toggle="modal" data-target="#settings" data-load-url="<?=base_url();?>admin_home/view/1">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">General Settings</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="#" data-toggle="modal" data-target="#settings" data-load-url="<?=base_url();?>admin_home/view/2">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-primary text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">About Us</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="#" data-toggle="modal" data-target="#settings" data-load-url="<?=base_url();?>admin_home/view/3">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">Terms & Conditions</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="<?=base_url();?>admin_home/author">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-primary text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">About Author</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="#" data-toggle="modal" data-target="#settings" data-load-url="<?=base_url();?>admin_home/view/4">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">Company Stats</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="<?=base_url();?>admin_home/testimonials">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-primary text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">Testimonials</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="<?=base_url();?>admin_home/logos">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">Logos</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="settings">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>