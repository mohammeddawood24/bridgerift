<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0">Company Stats <span class="text-danger">*</span></h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/update_settings');?>
<input type="hidden" name="page_id" value="<?=$page_id;?>">
<div class="modal-body">
    <div class="row">
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Professional Trained</label>
	            <input class="form-control" type="number" placeholder="Enter No." name="trained" required value="<?=($content)?$content->trained:'';?>">
	        </div>
	    </div>
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Fortune</label>
	            <input class="form-control" type="number" placeholder="Enter No." name="fortune" required value="<?=($content)?$content->fortune:'';?>">
	        </div>
	    </div>
	</div>
    <div class="row">
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Companies</label>
	            <input class="form-control" type="number" placeholder="Enter No." name="company" required value="<?=($content)?$content->company:'';?>">
	        </div>
	    </div>
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Certification on Business Modules</label>
	            <input class="form-control" type="number" placeholder="Enter No." name="certificate" required value="<?=($content)?$content->certificate:'';?>">
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>