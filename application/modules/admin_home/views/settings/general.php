<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0">General Settings</h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/update_settings');?>
<input type="hidden" name="page_id" value="<?=$page_id;?>">
<div class="modal-body">
    <div class="row">
	    <div class="col-6">
			<div class="form-group">
	            <label>Site Name <span class="text-danger">*</span></label>
	            <input class="form-control" type="text" placeholder="Enter Site Name" name="site_name" required value="<?=($content)?$content->site_name:'';?>">
	        </div>
	    </div>
	    <div class="col-6">
            <label>Contact Email <span class="text-danger">*</span></label>
            <input class="form-control" type="email" placeholder="Enter Contact Email" name="email" required value="<?=($content)?$content->email:'';?>">
        </div>
	</div>

	<div class="row">
		<div class="col-6">
			<div class="form-group">
	            <label>Contact No. <span class="text-danger">*</span></label>
	            <input class="form-control" type="number" placeholder="Enter Contact No." name="contact" required value="<?=($content)?$content->contact:'';?>">
	        </div>
		</div>
		<div class="col-6">
			<div class="form-group">
	            <label>Facebook URL <span class="text-danger">*</span></label>
	            <input class="form-control" type="text" placeholder="Enter FB URL" name="fb" required value="<?=($content)?$content->facebook_url:'';?>">
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<div class="form-group">
	            <label>Twitter URL <span class="text-danger">*</span></label>
	            <input class="form-control" type="text" placeholder="Enter Twitter URL" name="twitter" required value="<?=($content)?$content->twitter_url:'';?>">
	        </div>
		</div>
		<div class="col-6">
			<div class="form-group">
	            <label>Youtube Channel URL <span class="text-danger">*</span></label>
	            <input class="form-control" type="text" placeholder="Enter Youtube URL" name="youtube" required value="<?=($content)?$content->youtube_url:'';?>">
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<div class="form-group">
	            <label>LinkedIn URL <span class="text-danger">*</span></label>
	            <input class="form-control" type="text" placeholder="Enter LinkedIn URL" name="linkedin" required value="<?=($content)?$content->linked_url:'';?>">
	        </div>
		</div>
		<div class="col-6">
			<div class="form-group">
	            <label>Address <span class="text-danger">*</span></label>
	            <textarea rows="3" class="form-control" name="address" required><?=($content)?$content->address:'';?></textarea>
	        </div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>