<?=modules::run('common/header');?>
<style type="text/css">
    .delete{
        color: #fc5454;
        padding: 1px 6px;
        position: absolute;
        top: -15px;
        right: 3px;
        font-size: 18px;
    }
</style>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="#" data-load-url="<?=base_url();?>admin_home/logo_view" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-logo"><i class="fa fa-plus"></i> &nbsp; Add</a>
                    <?=anchor('admin_home/settings', '<i class="fa fa-reply"></i> &nbsp; Back', 'class="btn btn-sm btn-danger"');?>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <?php if ($logos) {
                    # code...
                    foreach ($logos as $key => $value) {
                        # code...
            ?>
            <div class="col-md-3">
                <img class="mr-2 client-logo img-fluid" alt="<?=$value->client_name;?>" src="<?=base_url().'/upload/logo/'.$value->logo;?>">
                <a href="<?=base_url();?>admin_home/del_logo/<?=$value->id;?>" class="delete"><i class="fas fa-times"></i></a>
            </div>
            <?php }}else { ?>
            <div class="col-md-12 text-center text-muted no-data-icon py-5">
                <i class="ti-files"></i>
                <div class="py-4">No logos uploaded yet...!!</div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-logo">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>