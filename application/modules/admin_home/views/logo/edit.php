<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0">Upload Logo</h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open_multipart('admin_home/logo_upload');?>

<div class="modal-body">
    <div class="row">
	    <div class="col-6">
	    	<div class="form-group">
	    		<label>Client Name <span class="text-danger">*</span></label>
	            <input type="text" name="client_name" class="form-control" required>
	        </div>
	    	<div class="form-group">
	    		<label>Upload Logo <span class="text-danger">*</span></label>
	            <input type="file" name="logo" class="form-control">
	        </div>
	    </div>
	    <div class="col-6">
	    	<?php $img = 'admin-asset/images/placeholder.jpg';
	    		if ($client && $client->img) {
					# code...
					$img = 'upload/'.$client->img;
				}
	    	?>
	    	<img src="<?=base_url().$img;?>" class="img-fluid">
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>