<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0"><?=$title;?></h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('admin_home/services/update');?>
<input type="hidden" name="page_id" value="<?=$page_id;?>">
<input type="hidden" name="section_id" value="<?=$section_id;?>">
<div class="modal-body">
    <div class="row">
	    <div class="col-12">
	    	<div class="form-group">
	            <textarea name="about" rows="12"><?=($service)?$service->about:'';?></textarea>
	        </div>
	    </div>
	</div>
    <div class="row">
	    <div class="col">
	    	<div class="form-group">
	    		<label>Youtube URL</label>
	            <input type="text" name="video_url" class="form-control" id="video_url" required value="<?=($service)?$service->video_url:'';?>">
	        </div>
	    </div>
	    <div class="col">
	    	<div class="form-group">
	    		<label>Skills</label>
	            <input type="text" name="skills" class="form-control" id="skills" required value="<?=($service)?$service->skills:'';?>">
	        </div>
	    </div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Save</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>