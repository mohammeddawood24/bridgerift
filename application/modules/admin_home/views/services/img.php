<div class="modal-header align-items-center">
	<h5 class="modal-title mt-0"><?=$title;?></h5>
	<button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open_multipart('admin_home/services/img_upload');?>
<input type="hidden" name="page_id" value="<?=$page_id;?>">
<div class="modal-body">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Upload Image</label>
				<input type="file" name="file" class="form-control">
			</div>
		</div>
		<div class="col-md-6">
			<?php 
				$img = 'admin-asset/images/placeholder.jpg';
				if ($service->img) {
					# code...
					$img = 'upload/'.$service->img;
				}
			?>
			<img src="<?=base_url().$img;?>" class="img-fluid">
		</div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" type="submit">Upload</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
</div>
<?=form_close();?>