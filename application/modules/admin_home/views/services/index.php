<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6">
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-md-3">
                <a href="<?=base_url();?>admin_home/services/view/1">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16 mb-1">Sales-Mantra</h5>
                                <p class="mb-0">&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="<?=base_url();?>admin_home/services/view/2">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-primary text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">Business-Edge Coaching</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3">
                <a href="<?=base_url();?>admin_home/services/view/3">
                    <div class="card mb-3">
                        <div class="card-heading p-4 text-center">
                            <div class="mini-stat-icon mb-4">
                                <i class="mdi mdi-buffer bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16">Boot-Camp for Professionals</h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>
<?=modules::run('common/footer');?>