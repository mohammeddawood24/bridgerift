<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#edit-service" data-load-url="<?=base_url();?>admin_home/services/img/<?=$page_id;?>"><i class="fa fa-upload"></i> &nbsp;Image</a>
                    <?=anchor('admin_home/services', '<i class="fa fa-reply"></i> Back', 'class="btn btn-danger"');?>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30 card-body">
                    <h4 class="card-title font-16 mt-0">About</h4>
                    <a href="#" data-toggle="modal" data-target="#edit-service" data-load-url="<?=base_url();?>admin_home/services/edit/<?=$page_id;?>/1" data-id="1" class="btn btn-danger service-edit-icon btn-sm"><i class="icon-pencil"></i></a>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text">
                                <?=($about)?$about->about:'<span class="text-muted">No data found...</span>';?>
                            </p>
                            <hr>
                            <h4 class="card-title font-16 mt-0">Skills</h4>
                            <?php if($about){
                                foreach(explode(',', $about->skills) as $key => $val){
                            ?>
                                <label class="badge badge-primary"><?=$val;?></label>
                            <?php }}else {?>
                                <span class="text-muted">No Data found..!!</span>
                            <?php }?>
                        </div>
                        <div class="col-md-6">
                            <?php if($about && $about->video_url){?>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="<?=$about->video_url;?>"></iframe>
                            </div>
                            <?php }else {?>
                                <img src="<?=base_url();?>admin-asset/images/placeholder.jpg" class="img-fluid">
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30 card-body">
                    <h4 class="card-title font-16 mt-0">Manage FAQ's</h4>
                    <a href="#" data-toggle="modal" data-target="#edit-service" data-load-url="<?=base_url();?>admin_home/services/edit/<?=$page_id;?>/2" data-id="2" class="btn btn-danger service-edit-icon btn-sm"><i class="icon-pencil"></i></a>
                    <table id="service-faq" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="45%">Question</th>
                                <th width="45%">Answer</th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($faqs){
                                    $i=0;
                                    foreach($faqs as $key => $val){
                                        $action = anchor('admin_home/services/delete/'.$page_id.'/2/'.$val->id, '<i class="icon-trash-bin text-danger"></i>', 'title="Delete"');
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td><?=$val->question;?></td>
                                <td><?=$val->answer;?></td>
                                <td class="text-center"><?=$action;?></td>
                            </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30 card-body">
                    <h4 class="card-title font-16 mt-0">Manage Testimonials</h4>
                    <a href="#" data-toggle="modal" data-target="#edit-service" data-load-url="<?=base_url();?>admin_home/services/edit/<?=$page_id;?>/3" data-id="3" class="btn btn-danger service-edit-icon btn-sm"><i class="icon-pencil"></i></a>
                    <table id="service-testimonials" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="20%">User Details</th>
                                <th width="60%">Message</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($testimonials){
                                    $i=0;
                                    foreach($testimonials as $key => $val){
                                        $action = anchor('admin_home/services/delete/'.$page_id.'/3/'.$val->id, '<i class="icon-trash-bin text-danger"></i>', 'title="Delete"');
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td>
                                    <?=$val->username;?><br>
                                    <span class="text-muted"><?=$val->designation;?></span>
                                </td>
                                <td><?=$val->testimonials;?></td>
                                <td class="text-center"><?=$action;?></td>
                            </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card m-b-30 card-body">
                    <h4 class="card-title font-16 mt-0">Manage Pricing</h4>
                    <a href="#" data-toggle="modal" data-target="#edit-service" data-load-url="<?=base_url();?>admin_home/services/edit/<?=$page_id;?>/4" data-id="4" class="btn btn-danger service-edit-icon btn-sm"><i class="icon-pencil"></i></a>
                    <table id="service-pricing" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="45%">Category</th>
                                <th width="15%">Basic</th>
                                <th width="15%">Silver</th>
                                <th width="15%">Star</th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($pricing){
                                    $i=0;
                                    foreach($pricing as $key => $val){
                                        $action = anchor('admin_home/services/delete/'.$page_id.'/4/'.$val->id, '<i class="icon-trash-bin text-danger"></i>', 'title="Delete"');
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td>
                                    <?=$val->category;?>
                                </td>
                                <td><?=$val->value1;?></td>
                                <td><?=$val->value2;?></td>
                                <td><?=$val->value3;?></td>
                                <td class="text-center"><?=$action;?></td>
                            </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-service">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>