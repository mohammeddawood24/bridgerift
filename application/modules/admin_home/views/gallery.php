<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6">
                    
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-md-12">
                <div class="card card-body table-responsive">
                    <table id="gallery" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Img</th>
                                <th>Name</th>
                                <th>Link</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($content) {
                                    # code...
                                    $i=0;
                                    foreach($content as $key => $value){
                                        # code...
                                        $img = base_url().'upload/'.$value->img;
                                        $action = anchor('admin_home/gallery_del/'.$value->id, '<i class="icon-trash-bin text-danger"></i>')
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td>
                                    <img src="<?=$img;?>" class="img-fluid img-sm">
                                </td>
                                <td><?=$value->name;?></td>
                                <td><?=anchor($value->link, 'Launch', 'target="_blank"');?></td>
                                <td><?=$action;?></td>
                            </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-gallery">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Gallery</h5>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <?=form_open_multipart('admin_home/gallery_edit');?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Upload Image <span class="text-danger">*</span></label>
                            <input type="file" name="img" class="form-control" id="img">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" id="name" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Link <span class="text-danger">*</span></label>
                            <input type="text" name="link" class="form-control" id="link" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
            </div>
            <?=form_close();?>
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>