<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Home extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
	}

	function index(){
		$this->data['title'] = 'Home Page';
		$this->load->view('home/index', $this->data);
	}
}