<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Admin_home extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
		$this->data['pg'] = 'setting';
		$this->load->model('site_settings_m');
		$this->load->model('gallery_m');
		$this->load->model('testimonials_m');
		$this->load->model('logo_m');
	}

	function index(){
		$this->data['title'] = 'Dashboard';
		$this->load->view('dashboard/index', $this->data);
	}

	function settings(){
		$this->data['title'] = 'Site Settings';
		
		if ($this->uri->segment(3) == 'updated') {
			# code...
			$data['msg'] = '';
			$data['cls'] = 'alert alert-success text-center';
		}else {
			$data['msg'] = '';
			$data['cls'] = '';
		}

		$this->load->view('settings/index', $this->data);
	}

	function view($page_id){
		$this->data['page_id'] = $page_id;
		$this->data['content'] = $this->site_settings_m->get(1);

		if ($page_id == 1) {
			# code...
			$this->load->view('settings/general', $this->data);
		}elseif ($page_id == 2) {
			# code...
			$this->load->view('settings/about', $this->data);
		}elseif ($page_id == 3) {
			# code...
			$this->load->view('settings/tc', $this->data);
		}elseif ($page_id == 4) {
			# code...
			$this->load->view('settings/stats', $this->data);
		}
	}

	function update_settings(){
		$page_id = $this->input->post('page_id');

		if ($page_id == 1) {
			# code...
			$data = array('site_name'=>$this->input->post('site_name'),
							'email' => $this->input->post('email'),
							'contact' => $this->input->post('contact'),
							'facebook_url' => $this->input->post('fb'),
							'twitter_url' => $this->input->post('twitter'),
							'youtube_url' => $this->input->post('youtube'),
							'linked_url' => $this->input->post('linkedin'),
							'address' => $this->input->post('address')
						);
			$this->site_settings_m->update(1, $data);
		}elseif ($page_id == 2) {
			# code...
			$data = array('about' => $this->input->post('about'));
			$this->site_settings_m->update(1, $data);
		}elseif ($page_id == 3) {
			# code...
			$data = array('terms_and_conditions' => $this->input->post('tc'));
			$this->site_settings_m->update(1, $data);
		}elseif ($page_id == 4) {
			# code...
			$data = array('trained' => $this->input->post('trained'),
							'fortune' => $this->input->post('fortune'),
							'company' => $this->input->post('company'),
							'certificate' => $this->input->post('certificate')
						);
			$this->site_settings_m->update(1, $data);
		}

		redirect('admin_home/settings/updated');
	}

	function gallery(){
		$this->data['title'] = 'Manage Gallery';
		$this->data['pg'] = 'gallery';

		$this->data['content'] = $this->gallery_m->get_all();
		
		$this->load->view('gallery', $this->data);
	}

	function gallery_edit(){
		// Thumbnail file upload config
	    $config['upload_path'] = './upload/'; //Upload Path
	    $config['allowed_types'] = 'jpg|png|jpeg';
	    $config['max_size'] = '5116'; //5MB
	    $config['max_width'] = '0';
	    $config['max_height'] = '0';

	    $this->load->library('upload', $config);
	    $pic = NULL;
	    if ($this->upload->do_upload('img')) {
			$this->data['upload_data'] = $this->upload->data();
			$pic = $this->data['upload_data']['file_name'];
	    }

	    $data = array('name'=>$this->input->post('name'),
						'link'=>$this->input->post('link'),
						'img'=>$pic,
						'created_on'=>date('Y-m-d'));
	    $this->gallery_m->insert($data);

	    redirect('admin_home/gallery');
	}

	function gallery_del($gallery_id){
		$this->gallery_m->delete($gallery_id);

		redirect('admin_home/gallery');
	}

	function testimonials(){
		$this->data['title'] = 'Manage Testimonials';
		$this->data['pg'] = 'testi';
		$this->data['testimonials'] = $this->testimonials_m->get_all();

		$this->load->view('testimonials/index', $this->data);
	}

	function testi_view($id = NULL){
		$this->data['testimonial'] = '';
		$this->data['testi_id'] = $id;
		if ($id) {
			# code...
			$this->data['testimonial'] = $this->testimonials_m->get($id);
		}

		$this->load->view('testimonials/edit', $this->data);
	}

	function edit(){
		$testi_id = $this->input->post('testi_id');

		$username = $this->input->post('username');
		$designation = $this->input->post('designation');
		$msg = $this->input->post('msg');

		$data = array('username'=>$username, 'designation'=>$designation, 'message'=>$msg);

		if ($testi_id) {
			# code...
			$this->testimonials_m->update($testi_id, $data);
		}else {
			$data['created_on'] = date('Y-m-d');
			$this->testimonials_m->insert($data);
		}

		redirect('admin_home/testimonials');
	}

	function testi_delete($id){
		$this->testimonials_m->delete($id);

		redirect('admin_home/testimonials');
	}

	function logos(){
		$this->data['title'] = 'Manage Logos';
		$this->data['pg'] = 'client_logo';
		$this->data['logos'] = $this->logo_m->get_all();

		$this->load->view('logo/index', $this->data);
	}

	function logo_view($id = NULL){
		$this->data['pg'] = 'client_logo';
		$this->data['client'] = '';
		if ($id) {
			# code...
			$this->data['client'] = $this->logo_m->get($id);
		}

		$this->load->view('logo/edit', $this->data);
	}

	function logo_upload(){
		// Thumbnail file upload config
	    $config['upload_path'] = './upload/logo/'; //Upload Path
	    $config['allowed_types'] = 'jpg|png|jpeg';
	    $config['max_size'] = '5116'; //5MB
	    $config['max_width'] = '0';
	    $config['max_height'] = '0';

	    $this->load->library('upload', $config);
	    $pic = NULL;
	    if ($this->upload->do_upload('logo')) {
			$this->data['upload_data'] = $this->upload->data();
			$pic = $this->data['upload_data']['file_name'];
	    }

	    $data = array('client_name'=>$this->input->post('client_name'),
						'logo'=>$pic,
						'created_on'=>date('Y-m-d'));
	    $this->logo_m->insert($data);

	    redirect('admin_home/logos');
	}
}