<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Services extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
		$this->data['pg'] = 'service';
		$this->load->model('services_m');
		$this->load->model('service_faq_m');
		$this->load->model('service_testimonials_m');
		$this->load->model('service_pricing_m');
	}

	function index(){
		$this->data['title'] = 'Services';
		$this->load->view('services/index', $this->data);
	}

	function view($page_id){
		$this->data['page_id'] = $page_id;
		
		$this->data['about'] = $this->services_m->get_by(array('service_id'=>$page_id));
		$this->data['faqs'] = $this->service_faq_m->get_many_by(array('service_id'=>$page_id));
		$this->data['testimonials'] = $this->service_testimonials_m->get_many_by(array('service_id'=>$page_id));
		$this->data['pricing'] = $this->service_pricing_m->get_many_by(array('service_id'=>$page_id));

		if ($page_id == 1) {
			# code...
			$this->data['title'] = 'Sales-Mantra';
		}elseif ($page_id == 2) {
			# code...
			$this->data['title'] = 'Business-Edge Coaching';
		}elseif ($page_id == 3) {
			# code...
			$this->data['title'] = 'Boot-Camp for Professionals';
		}
		$this->load->view('services/service_view', $this->data);
	}

	function edit($page_id, $section_id){
		$this->data['page_id'] = $page_id;
		$this->data['section_id'] = $section_id;

		if ($section_id == 1) {
			# code...
			$this->data['service'] = $this->services_m->get_by(array('service_id'=>$page_id));
			$this->data['title'] = 'About';

			$this->load->view('services/about_section', $this->data);
		}elseif ($section_id == 2) {
			# code...
			$this->data['title'] = 'Manage FAQ\'s';

			$this->load->view('services/faq_section', $this->data);
		}elseif ($section_id == 3) {
			# code...
			$this->data['title'] = 'Manage Testimonials';

			$this->load->view('services/testimonial_section', $this->data);
		}elseif ($section_id == 4) {
			# code...
			$this->data['title'] = 'Manage Pricing';

			$this->load->view('services/pricing_section', $this->data);
		}
	}

	function update(){
		$page_id = $this->input->post('page_id');
		$section_id = $this->input->post('section_id');

		if ($section_id == 1) {
			# code...
			$det = $this->services_m->get_by(array('service_id'=>$page_id));

			$about = $this->input->post('about');
			$skills = $this->input->post('skills');
			$video_url = $this->input->post('video_url');

			$data = array('about'=>$about, 'skills'=>$skills, 'video_url'=>$video_url, 'created_on'=>date('Y-m-d'), 'updated_by'=>$this->data['user']['id']);
			
			if ($det) {
				# code...
				$this->services_m->update($det->id, $data);
			}else {
				$data['service_id'] = $page_id;
				$this->services_m->insert($data);
			}
		}elseif ($section_id == 2) {
			$que = $this->input->post('que');
			$ans = $this->input->post('ans');

			$data = array('service_id'=>$page_id, 'question'=>$que, 'answer'=>$ans);
			$this->service_faq_m->insert($data);
		}elseif ($section_id == 3) {
			$username = $this->input->post('username');
			$designation = $this->input->post('designation');
			$msg = $this->input->post('msg');

			$data = array('service_id'=>$page_id, 'username'=>$username, 'designation'=>$designation, 'testimonials'=>$msg, 'created_on'=>date('Y-m-d'));
			$this->service_testimonials_m->insert($data);
		}elseif ($section_id == 4) {
			$data = array('service_id'=>$page_id,
							'category'=>$this->input->post('cat'),
							'value1'=>$this->input->post('val1'),
							'value2'=>$this->input->post('val2'),
							'value3'=>$this->input->post('val3'));
			$this->service_pricing_m->insert($data);
		}

		redirect('admin_home/services/view/'.$page_id);
	}

	function delete($page_id, $section_id, $item_id){
		if ($section_id == 2) {
			# code...
			$this->service_faq_m->delete($item_id);
		}elseif ($section_id == 3) {
			$this->service_testimonials_m->delete($item_id);
		}elseif ($section_id == 4) {
			$this->service_pricing_m->delete($item_id);
		}

		redirect('admin_home/services/view/'.$page_id);
	}

	function img($page_id){
		$this->data['page_id'] = $page_id;
		$this->data['title'] = 'Image Upload';

		$this->data['service'] = $this->services_m->get_by(array('service_id'=>$page_id));

		$this->load->view('services/img', $this->data);
	}

	function img_upload(){
		$page_id = $this->input->post('page_id');
		$this->services_m->_select = 'id';
		$service = $this->services_m->get_by(array('service_id'=>$page_id));

		// Thumbnail file upload config
	    $config['upload_path'] = './upload/'; //Upload Path
	    $config['allowed_types'] = 'jpg|png|jpeg';
	    $config['max_size'] = '10232'; //10MB

	    $this->load->library('upload', $config);
	    $pic = NULL;
	    if ($this->upload->do_upload('file')) {
			$this->data['upload_data'] = $this->upload->data();
			$pic = $this->data['upload_data']['file_name'];
	    }
	    
	    $data = array('img'=>$pic);
	    $this->services_m->update($service->id, $data);


		redirect('admin_home/services/view/'.$page_id);
	}
}