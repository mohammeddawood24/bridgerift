<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Manage_users extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
	}

	function index(){
		$this->load->view('user/index');
	}
}