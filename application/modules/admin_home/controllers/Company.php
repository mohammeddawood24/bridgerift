<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Company extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
		$this->data['pg'] = 'company';
		$this->load->model('company_m');
		$this->load->model('company_contact_m');
	}

	function index(){
		$this->data['title'] = 'Manage Companies';
		$this->data['content'] = $this->company_m->get_all();

		$this->load->view('company/index', $this->data);
	}

	function view($company_id = NULL){
		$this->data['company_id'] = $company_id;
		$this->data['content'] = '';
		
		if ($company_id) {
			# code...
			$this->data['content'] = $this->company_m->get($company_id);
		}

		$this->load->view('company/edit', $this->data);
	}

	function edit(){
		$company_id = $this->input->post('company_id');

		$data = array('name'=>$this->input->post('name'),
					'website'=>$this->input->post('website'));

		if ($company_id) {
			# code...
			$this->company_m->update($company_id, $data);
		}else {
			$data['created'] = current_date();
			$company_id = $this->company_m->insert($data);
		}

		redirect('admin_home/company');
	}

	function contacts($company_id = NULL){
		$company = $this->company_m->get($company_id);
		$this->data['title'] = 'Manage Company Contacts - '.$company->name;
		$this->data['company_id'] = $company_id;

		$this->data['content'] = $this->company_contact_m->get_many_by(array('company_id'=>$company_id));

		$this->load->view('company/contacts', $this->data);
	}

	function contact_view($company_id, $contact_id = NULL){
		$this->data['contact_id'] = $contact_id;
		$this->data['company_id'] = $company_id;
		$this->data['content'] = '';
		
		if ($contact_id) {
			# code...
			$this->data['content'] = $this->company_contact_m->get($contact_id);
		}

		$this->load->view('company/contact_edit', $this->data);
	}

	function contact_edit(){
		$company_id = $this->input->post('company_id');
		$contact_id = $this->input->post('contact_id');

		$data = array('company_id'=>$this->input->post('company_id'),
					'name'=>$this->input->post('name'),
					'email'=>$this->input->post('email'),
					'mobile'=>$this->input->post('mobile'),
					'mobile1'=>$this->input->post('mobile1'),
					'skype'=>$this->input->post('skype'));

		if ($contact_id) {
			# code...
			$this->company_contact_m->update($contact_id, $data);
		}else {
			$data['created'] = current_date();
			$contact_id = $this->company_contact_m->insert($data);
		}

		redirect('admin_home/company/contacts/'.$company_id);
	}

	function get_company_contacts($company_id){
		$contacts = get_company_contacts($company_id);

		echo form_dropdown('contact_id', $contacts, '', 'class="form-control" id="contact_id"');
		die;
	}
}