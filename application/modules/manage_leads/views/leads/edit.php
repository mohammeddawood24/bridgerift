<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
    	<?=form_open('manage_leads/add');?>
    	<input type="hidden" name="lead_id" value="<?=$lead_id;?>">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title">Lead Information</h4>
                </div>
                <div class="col-sm-6 text-right">
                	<button type="submit" class="btn btn-primary btn-sm"><?=($lead)?'Update':'Save'?></button>
                	<?=anchor('manage_leads', 'Cancel', 'class="btn btn-sm btn-danger"');?>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <div class="row">
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Title <span class="text-danger">*</span></label>
					            <input type="text" name="lead_name" class="form-control" required placeholder="Enter lead name" value="<?=($lead)?$lead->lead_name:'';?>">
					        </div>
					    </div>
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Company Name <span class="text-danger">*</span></label>
					            <?=form_dropdown('company_id', get_company(), ($lead)?$lead->company_id:'', 'class="form-control" id="company_id"');?>
					        </div>
					    </div>
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Contact Person Name <span class="text-danger">*</span></label>
					            <select class="form-control" name="contact_id" id="contact_id">
					            	<option>--Select Company First--</option>
					            </select>
					        </div>
					    </div>
					</div>
					<div class="row">
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Lead Owner <span class="text-danger">*</span></label>
					            <?=form_dropdown('lead_owner', $lead_owner,  (($lead)?$lead->lead_owner:''), 'class="form-control select2" id="lead_owner"');?>
					        </div>
					    </div>
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Lead Source </label>
					            <?=form_dropdown('lead_source', lead_source(),  (($lead)?$lead->lead_source:''), 'class="form-control" id="lead_source"');?>
					        </div>
					    </div>
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Lead Status <span class="text-danger">*</span></label>
					            <?=form_dropdown('lead_status', lead_status(),  (($lead)?$lead->lead_status:''), 'class="form-control" id="lead_status"');?>
					        </div>
					    </div>
					</div>
					<div class="row">
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Estimation</label>
					            <input type="number" step="any" name="price" placeholder="Enter estimated price (INR)" class="form-control" value="<?=($lead)?$lead->price:'';?>">
					        </div>
					    </div>
					    <div class="col-4">
					    	<div class="form-group">
					    		<label>Pridict Closing date</label>
					            <input class="form-control" type="date" name="due_date" value="" id="example-date-input">
					        </div>
					    </div>
					</div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
    			<h5 class="custom-page-title">Address Information</h5>
        		<div class="card card-body">
        			<div class="row">
        				<div class="col-12">
        					<div class="form-group">
					    		<label>Address Line 1</label>
					            <textarea class="form-control" rows="3" name="address" placeholder="Enter full address"> <?=($lead)?$lead->address:'';?></textarea>
					        </div>
        				</div>
        			</div>
        			<div class="row">
        				<div class="col-3">
        					<div class="form-group">
					    		<label>City</label>
					            <input type="text" name="city" placeholder="Enter city" class="form-control" value="<?=($lead)?$lead->city:'';?>">
					        </div>
        				</div>
        				<div class="col-3">
        					<div class="form-group">
					    		<label>State</label>
					            <input type="text" name="state" placeholder="Enter state" class="form-control" value="<?=($lead)?$lead->state:'';?>">
					        </div>
        				</div>
        				<div class="col-3">
        					<div class="form-group">
					    		<label>Country</label>
					            <input type="text" name="country" placeholder="Enter country" class="form-control" value="<?=($lead)?$lead->country:'';?>">
					        </div>
        				</div>
        				<div class="col-3">
        					<div class="form-group">
					    		<label>Zipcode</label>
					            <input type="number" maxlength="6" minlength="6" name="zipcode" placeholder="Enter Area Zipcode" class="form-control" value="<?=($lead)?$lead->zipcode:'';?>">
					        </div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
    			<h5 class="custom-page-title">Lead Description</h5>
        		<div class="card card-body">
        			<div class="row">
        				<div class="col-12">
        					<div class="form-group">
					    		<label>Description <span class="text-danger">*</span></label>
					            <textarea class="form-control" name="desc" placeholder="Enter lead description" rows="4" required><?=($lead)?$lead->description:'';?></textarea>
					        </div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <?=form_close();?>
    </div>
</div>
<?=modules::run('common/footer');?>