<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary">Initiate Lead Process</button>
                    <?=anchor('manage_leads', '<i class="fas fa-long-arrow-alt-left"></i> Back', 'class="btn btn-sm btn-danger"')?>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->
        <?php $company = $this->company_m->get($lead->company_id);
            $contact = $this->company_contact_m->get($lead->contact_person_id);
        ?>
        <div class="row">
            <div class="col-8">
                <div class="card card-body faq-box border-success">
                    <div class="row">
                        <div class="col-sm-3 text-right">Lead Owner</div>
                        <div class="col-sm-8"><?=$lead_owner[$lead->lead_owner];?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3 text-right">Email</div>
                        <div class="col-sm-8 text-primary"><?=$contact->email;?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3 text-right">Phone</div>
                        <div class="col-sm-8"><?=$contact->mobile;?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3 text-right">Lead Status</div>
                        <div class="col-sm-8"><?=lead_status($lead->lead_status);?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3 text-right">Estimated Price</div>
                        <div class="col-sm-8">Rs. <?=number_format($lead->price, 2);?>/-</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3 text-right">Estimated Date of Completion</div>
                        <div class="col-sm-8"><?=date('d-M-y', strtotime($lead->due_date));?></div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <?php $dues = $this->lead_task_m->get_many_by(array('status'=>0));
                    if($dues){
                ?>

                <div class="card faq-box border-danger">
                    <div class="card-body py-1">
                        <h5 class="custom-card-title mt-1">Next Action</h5>
                        <div class="scrollbox">
                            <div class="scrollbox-content">
                                <?php foreach($dues as $due){
                                        if ($due->due_date < date('Y-m-d')) {
                                            # code...
                                            $cls = 'ribbon-danger';
                                        }elseif ($due->due_date == date('Y-m-d')) {
                                            # code...
                                            $cls = 'ribbon-warning';
                                        }elseif ($due->due_date > date('Y-m-d')) {
                                            # code...
                                            $cls = 'ribbon-success';
                                        }
                                ?>
                                <div class="mb-1">
                                    <span class="ribbon <?=$cls;?>">
                                        <?=date('M d', strtotime($due->due_date))." - ".ucwords($due->subject);?>
                                    </span>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="custom-page-title">Lead Information</h5>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#lead-details">
                                Show Details
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="collapse" id="lead-details">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-sm-4 text-right">Lead Name</div>
                                    <div class="col-sm-8"><?=$lead->lead_name;?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Primary Contact No.</div>
                                    <div class="col-sm-8"><?=$contact->mobile;?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Lead Status</div>
                                    <div class="col-sm-8"><?=lead_status($lead->lead_status);?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Lead Source</div>
                                    <div class="col-sm-8"><?=lead_source($lead->lead_source);?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Modified By</div>
                                    <div class="col-sm-8">
                                        <?=$lead_owner[$lead->updated_by];?><br>
                                        <?=date('d M, y', strtotime($lead->updated_on));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-sm-4 text-right">Company Name</div>
                                    <div class="col-sm-8"><?=$company->name;?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Contact Person</div>
                                    <div class="col-sm-8"><?=$contact->name;?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Secondary Contact No.</div>
                                    <div class="col-sm-8"><?=($contact->mobile1)?$contact->mobile1:'-';?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Email</div>
                                    <div class="col-sm-8"><?=$contact->email;?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Website</div>
                                    <div class="col-sm-8"><?=$company->website;?></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4 text-right">Skype ID</div>
                                    <div class="col-sm-8"><?=$contact->skype;?></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5 class="custom-page-title">Address Information</h5>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3 text-right">Address</div>
                                    <div class="col-sm-9"><?=$lead->address;?></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3 text-right">City</div>
                                    <div class="col-sm-9"><?=$lead->city;?></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3 text-right">State</div>
                                    <div class="col-sm-9"><?=$lead->state;?></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3 text-right">Country</div>
                                    <div class="col-sm-9"><?=$lead->country;?></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3 text-right">Zipcode</div>
                                    <div class="col-sm-9"><?=$lead->zipcode;?></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5 class="custom-page-title">Description Information</h5>
                        <div class="row">
                            <div class="col-sm-12"><?=$lead->description;?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $open_tasks = $this->lead_task_m->get_many_by(array('status'=>0));?>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-brand-yellow">
                        <h4 class="custom-page-title m-0 d-inline-block">Open Activities</h4>
                        <button class="btn btn-light btn-sm float-right" data-toggle="modal" data-target="#edit-task" data-load-url="<?=base_url();?>manage_leads/view_task/<?=$lead_id;?>"><i class="fa fa-plus"></i> Task</button>
                    </div>
                    <div class="card-body">
                        <?php if($open_tasks){?>
                        <div class="row">
                            <div class="col table-responsive">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%">Subject</th>
                                            <th>Activity Type</th>
                                            <th>Due Date</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Modified Time</th>
                                            <th>Priority</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($open_tasks as $task) {
                                            # code...
                                            if ($task->priority == 0 || $task->priority == 1) {
                                                # code...
                                                $color = 'danger';
                                            }elseif ($task->priority == 2 || $task->priority == 3) {
                                                $color = 'info';
                                            }else {
                                                $color = 'success';
                                            }
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#edit-task" data-load-url="<?=base_url();?>manage_leads/view_task/<?=$lead_id;?>/<?=$task->id;?>"><i class="fas fa-edit text-primary"></i></a>
                                                <a href="<?=base_url();?>manage_leads/update_task/<?=$task->id;?>" data-toggle="tooltip" data-original-title="Close Task"><i class="fas fa-check-circle text-green-500 close-task"></i></a> &nbsp;
                                                <a href="" data-toggle="modal" data-target="#edit-task" data-load-url="<?=base_url();?>manage_leads/task_desc/<?=$task->id;?>"><?=ucwords($task->subject);?> <i class="fas fa-link"></i></a>
                                            </td>
                                            <td><?=activity_type($task->activity_type);?></td>
                                            <td><?=lead_date_format($task->due_date);?></td>
                                            <td class="text-center"><?=($task->activity_type)?lead_time_format($task->time_from):'-';?></td>
                                            <td class="text-center"><?=($task->activity_type)?lead_time_format($task->time_to):'-';?></td>
                                            <td><?=$task->created_on;?></td>
                                            <td class="text-center">
                                                <label class="badge badge-<?=$color;?> p-2"><?=priority($task->priority);?></span>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php }else {
                                echo 'No Data found';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $open_tasks = $this->lead_task_m->get_many_by(array('status'=>1));?>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-brand-yellow">
                        <h4 class="custom-page-title m-0 d-inline-block">Closed Activities</h4>
                    </div>
                    <div class="card-body">
                        <?php if($open_tasks){?>
                        <div class="row">
                            <div class="col table-responsive">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%">Subject</th>
                                            <th>Activity Type</th>
                                            <th>Due Date</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Modified Time</th>
                                            <th>Priority</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($open_tasks as $task) {
                                            # code...
                                            if ($task->priority == 0 || $task->priority == 1) {
                                                # code...
                                                $color = 'danger';
                                            }elseif ($task->priority == 2 || $task->priority == 3) {
                                                $color = 'info';
                                            }else {
                                                $color = 'success';
                                            }
                                        ?>
                                        <tr>
                                            <td>
                                                <!-- <a href="#" data-toggle="modal" data-target="#edit-task" data-load-url="<?=base_url();?>manage_leads/view_task/<?=$lead_id;?>/<?=$task->id;?>"><i class="fas fa-edit text-primary"></i></a> &nbsp; -->
                                                <a href="" data-toggle="modal" data-target="#edit-task" data-load-url="<?=base_url();?>manage_leads/task_desc/<?=$task->id;?>"><?=ucwords($task->subject);?> <i class="fas fa-link"></i></a>
                                            </td>
                                            <td><?=activity_type($task->activity_type);?></td>
                                            <td><?=lead_date_format($task->due_date);?></td>
                                            <td class="text-center"><?=($task->activity_type)?lead_time_format($task->time_from):'-';?></td>
                                            <td class="text-center"><?=($task->activity_type)?lead_time_format($task->time_to):'-';?></td>
                                            <td><?=$task->created_on;?></td>
                                            <td class="text-center">
                                                <label class="badge badge-<?=$color;?> p-2"><?=priority($task->priority);?></span>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php }else {
                                echo 'No Data found';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-task" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>