<?=modules::run('common/header');?>
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <h4 class="page-title"><?=$title;?></h4>
                </div>
                <div class="col-sm-6 text-right">
                    <?=anchor('manage_leads/add', '<i class="fa fa-plus"></i> Add', 'class="btn btn-sm btn-primary"');?>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end page-title -->

        <div class="row">
            <div class="col-md-12">
                <div class="card card-body table-responsive">
                    <input type="hidden" name="page_id" id="page_id" value="<?=$page_id;?>">
                    <table id="leads" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Lead Name</th>
                                <th>Company</th>
                                <th>Contact Person</th>
                                <th>Lead Source</th>
                                <th>Lead Owner</th>
                                <th>Lead Status</th>
                                <?php if($user['role'] == 0) {?>
                                <th>Action</th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0;
                                foreach ($content as $key => $value) {
                                    # code...
                                    $actions = anchor('manage_leads/add/'.$value->id, '<i class="icon-pencil"></i>', 'class="text-primary"');
                            ?>
                            <tr>
                                <td><?=++$i;?></td>
                                <td><?=anchor('manage_leads/view/'.$value->id, $value->lead_name, 'class="text-primary"');?></td>
                                <td><?=get_company($value->company_id);?></td>
                                <td><?=get_company_contacts($value->company_id, $value->contact_person_id);?></td>
                                <td><?=lead_source($value->lead_source);?></td>
                                <td><?=$lead_owner[$value->lead_owner];?></td>
                                <td><?=lead_status($value->lead_status);?></td>
                                <?php if($user['role'] == 0) {?>
                                <td><?=$actions;?></td>
                                <?php }?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>