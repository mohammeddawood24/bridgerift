<div class="modal-header align-items-center">
    <h5 class="modal-title mt-0">Task</h5>
    <button class="close" data-dismiss="modal">&times;</button>
</div>
<?=form_open('manage_leads/edit_task','name="form"');?>
<input type="hidden" name="lead_id" value="<?=$lead_id;?>">
<input type="hidden" name="task_id" value="<?=$task_id;?>">
<div class="modal-body">
    <div class="row">
        <div class="col-6 form-group">
            <label>Subject</label>
            <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter subject">
        </div>
        <div class="col-6 form-group">
            <label>Activity Type</label>
            <?=form_dropdown('activity_type', activity_type(), 0, 'class="form-control" id="activity_type"');?>
        </div>
    </div>
    <div class="row">
        <div class="col-6 form-group">
            <label>Due Date</label>
            <input class="form-control" type="date" name="due_date" value="<?=date('Y-m-d');?>" id="example-date-input">
        </div>
        <div class="col-6 form-group">
            <label>Priority</label>
            <?=form_dropdown('priority', priority(), 0, 'class="form-control" id="priority"');?>
        </div>
    </div>
    <div class="row" id="time-row">
        <div class="col-4 form-group">
            <label>From</label>
            <input class="form-control" type="time" name="from" id="example-time-input">
        </div>
        <div class="col-4 form-group">
            <label>To</label>
            <input class="form-control" type="time" name="to" id="example-time-input">
        </div>
        <div class="col-4 form-group">
            <label>Location</label>
            <input type="text" name="location" class="form-control" placeholder="Enter location">
        </div>
    </div>
    <div class="row">
        <div class="col-12 form-group">
            <label>Description</label>
            <textarea class="form-control" rows="3" name="desc" placeholder="Enter desc here"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <button type="submit" class="btn btn-sm btn-primary">Save</button>
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>
<?=form_close();?>