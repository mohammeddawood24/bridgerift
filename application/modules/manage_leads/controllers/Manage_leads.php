<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 * 
 */
class Manage_leads extends MX_Controller{
	private $data;
	function __construct(){
		# code...
		parent::__construct();
		$this->data['user'] = require_user();
		$this->data['pg'] = 'lead';
		$this->load->model('leads_m');
		$this->load->model('lead_task_m');
		$this->load->model('user_m');
		$this->load->model('company_m');
		$this->load->model('company_contact_m');
	}

	private function users(){
		$users = $this->user_m->get_many_by(array('status'=>1));
		
		$opt = array(''=>'--Select Lead Owner--');
		foreach ($users as $key => $value) {
			# code...
			$opt += array($value->id => $value->name);
		}

		return $opt;
	}

	function index(){
		$this->data['title'] = 'Manage Leads';
		$this->data['page_id'] = '1';

		$this->data['content'] = $this->leads_m->get_all();
		$this->data['lead_owner'] = $this->users();

		$this->load->view('leads/index', $this->data);
	}

	function view($id = NULL){
		$this->data['lead_id'] = $id;
		$this->data['lead'] = $this->leads_m->get($id);
		$this->data['title'] = 'Lead - '.$this->data['lead']->lead_name;

		$this->data['lead_owner'] = $this->users();

		$this->load->view('leads/view', $this->data);
	}

	function add($lead_id = NULL){
		$this->data['title'] = 'Manage Leads';
		$this->data['lead_owner'] = $this->users();
		$this->data['lead_id'] = $lead_id;

		if ($lead_id) {
			# code...
			$this->data['lead'] = $this->leads_m->get($lead_id);
		}else{
			$this->data['lead'] = '';
		}
		
		$this->form_validation->set_rules('lead_name', 'Lead Name', 'trim|xss_clean');
		if ($this->form_validation->run() == FALSE) {
			# code...
			$this->load->view('leads/edit', $this->data);
		}else {
			$lead_id = $this->input->post('lead_id');
			$data = array('lead_name'=>$this->input->post('lead_name'), 
						'company_name'=>$this->input->post('company'), 
						'contact_person'=>$this->input->post('contact_name'), 
						'email'=>$this->input->post('email'), 
						'mobile1'=>$this->input->post('mobile1'), 
						'mobile2'=>$this->input->post('mobile2'), 
						'lead_source'=>$this->input->post('lead_source'), 
						'website'=>$this->input->post('website'), 
						'skype_id'=>$this->input->post('skype_id'), 
						'lead_status'=>$this->input->post('lead_status'), 
						'lead_owner'=>$this->input->post('lead_owner'), 
						'address'=>$this->input->post('address'), 
						'state'=>$this->input->post('state'), 
						'city'=>$this->input->post('city'), 
						'zipcode'=>$this->input->post('zipcode'), 
						'country'=>$this->input->post('country'), 
						'description'=>$this->input->post('desc'));
			if ($lead_id) {
				# code...
				$data['updated_by'] = $this->data['user']['id'];
				$data['updated_on'] = date('Y-m-d');

				$this->leads_m->update($lead_id, $data);
			}else {
				$data['updated_by'] = $this->data['user']['id'];
				$data['updated_on'] = date('Y-m-d');

				$this->leads_m->insert($data);
			}

			redirect('manage_leads');
		}
	}

	function view_task($lead_id = NULL, $task_id = NULL){
		$this->data['lead_id'] = $lead_id;
		$this->data['task_id'] = $task_id;
		$this->data['task'] = '';
		if ($task_id) {
			# code...
			$this->data['task'] = $this->lead_task_m->get($task_id);
		}

		$this->load->view('leads/edit_task', $this->data);
	}

	function task_desc($task_id = NULL){
		$this->lead_task_m->_select = 'description';
		$this->data['task'] = $this->lead_task_m->get($task_id);

		$this->load->view('leads/view_task_desc', $this->data);
	}

	function edit_task(){
		$task_id = $this->input->post('task_id');
		$lead_id = $this->input->post('lead_id');

		$data = array('lead_id'=>$this->input->post('lead_id'),
						'subject'=>$this->input->post('subject'),
						'activity_type'=>$this->input->post('activity_type'),
						'due_date'=>$this->input->post('due_date'),
						'priority'=>$this->input->post('priority'),
						'description'=>$this->input->post('desc'));

		if($this->input->post('activity_type') != 0){
			$data['time_from'] = $this->input->post('from');
			$data['time_to'] = $this->input->post('to');
			$data['location'] = $this->input->post('location');
		}

		if (!$task_id) {
			# code...
			$data['created_by'] = $this->data['user']['id'];
			$data['created_on'] = current_date();

			$this->lead_task_m->insert($data);
		}else {
			$data['updated_by'] = $this->data['user']['id'];
			$data['updated_on'] = current_date();
			
			$this->lead_task_m->update($task_id, $data);
		}

		redirect('manage_leads/view/'.$lead_id);
	}

	function update_task($task_id){
		$task = $this->lead_task_m->get($task_id);
		$this->lead_task_m->update($task_id, array('status'=>1));

		redirect('manage_leads/view/'.$task->lead_id);
	}

	function search($page_id){
		$limit = $this->input->post('length');
		$offset = $this->input->post('start');
		$columns = $this->input->post('columns');
		$search = $this->input->post('search');
		$order = $this->input->post('order');

		$results = $this->leads_m->search($search, $columns, $order, $limit, $offset, $page_id);

		if ($results) {
			$base_url = base_url();
			foreach ($results as $result) {
				
			}
		}

		$total = $this->leads_m->search_count($search, $columns, $page_id);
		echo json_encode(array('data' => $results,
				"recordsTotal" => $total,
				"recordsFiltered" => $total,
			));
		exit;
	}
}