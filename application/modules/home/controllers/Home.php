<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	private $data;
	function __construct(){
		# code...
		parent :: __construct();
		$this->load->model('website_m');
		$this->load->model('services_m');
		$this->load->model('gallery_m');
		$this->load->model('testimonials_m');
	}

	function index() {
		$this->data['site'] = $this->website_m->get_site_details();
		$this->data['service'] = $this->services_m->get_all();
		$this->data['gallery'] = $this->gallery_m->get_all();
		$this->data['testimonials'] = $this->testimonials_m->get_all();

		$this->load->view('index', $this->data);
	}

	function service($slug) {
		$this->data['site'] = $this->website_m->get_site_details();
		$this->data['service'] = $this->website_m->service_det($slug);
		$this->data['service_faq'] = $this->website_m->service_faq($this->data['service']->id);
		$this->data['service_testi'] = $this->website_m->service_testi($this->data['service']->id);
		$this->data['service_pricing'] = $this->website_m->service_pricing($this->data['service']->id);

		$this->load->view('service', $this->data);
	}
}
