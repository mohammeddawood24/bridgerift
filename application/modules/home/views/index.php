<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="theme-color" content="#E31212">
	<meta name="author" description="Mohammed Dawood">
	<meta name="keywords" description="Sales Training, Sales CRM">

	<link rel="shortcut icon" href="./favicon.png" />
	
	<title>BridgeRift</title>

	<link href="//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600&display=swap" rel="stylesheet">
	
	<link rel="stylesheet" href="<?=base_url();?>assets/vendors/liquid-icon/liquid-icon.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/theme-vendors.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/theme.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/themes/creative.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css" />
	
	<!-- Head Libs -->
	<script async src="<?=base_url();?>assets/vendors/modernizr.min.js"></script>
	
</head>
<body data-mobile-nav-trigger-alignment="right" data-mobile-nav-align="left" data-mobile-nav-style="modern" data-mobile-nav-shceme="gray" data-mobile-header-scheme="gray" data-mobile-nav-breakpoint="1199">
	
	<div id="wrap">
		
		<header class="main-header main-header-overlay" data-sticky-header="true" data-sticky-options='{ "stickyTrigger": "first-section" }'>
			
			<div class="mainbar-wrap">
				<div class="megamenu-hover-bg"></div><!-- /.megamenu-hover-bg -->
				<div class="container-fluid mainbar-container">
					<div class="mainbar">
						<div class="row mainbar-row align-items-lg-stretch px-4">
							
							<div class="col-auto pr-5">
								<div class="navbar-header">
									<a class="navbar-brand" href="<?=base_url();?>" rel="home">
										<span class="navbar-brand-inner">
											<img class="logo-dark custom-logo" src="<?=base_url();?>assets/img/logo.png" alt="BridgeRift Sales Consultant">
											<img class="logo-default custom-logo" src="<?=base_url();?>assets/img/logo.png" alt="BridgeRift Sales Consultant">
										</span>
									</a>
									<button type="button" class="navbar-toggle collapsed nav-trigger style-mobile" data-toggle="collapse" data-target="#main-header-collapse" aria-expanded="false" data-changeclassnames='{ "html": "mobile-nav-activated overflow-hidden" }'>
										<span class="sr-only">Toggle navigation</span>
										<span class="bars">
											<span class="bar"></span>
											<span class="bar"></span>
											<span class="bar"></span>
										</span>
									</button>
								</div><!-- /.navbar-header -->
							</div><!-- /.col -->
							
							<div class="col">
								
								<div class="collapse navbar-collapse" id="main-header-collapse">
									
									<ul id="primary-nav" class="main-nav nav align-items-lg-stretch justify-content-lg-start custom-nav" data-submenu-options='{ "toggleType":"fade", "handler":"mouse-in-out" }' data-localscroll="true">
										
										<li>
											<a href="#content">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">Home</span>
												</span>
											</a>
										</li>
										<li>
											<a href="#about">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">About</span>
												</span>
											</a>
										</li>
										<li>
											<a href="#gallery">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">Gallery</span>
												</span>
											</a>
										</li>
										<li>
											<a href="#services">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">Services</span>
												</span>
											</a>
										</li>
										<li>
											<a href="#clients">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">Clients</span>
												</span>
											</a>
										</li>
										<li>
											<a href="#testimonials">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">Testimonials</span>
												</span>
											</a>
										</li>
										<li>
											<a href="#blog">
												<span class="link-icon"></span>
												<span class="link-txt">
													<span class="link-ext"></span>
													<span class="txt">Blog</span>
												</span>
											</a>
										</li>
									
									</ul><!-- /#primary-nav  -->
								
								</div><!-- /#main-header-collapse -->
								
							</div><!-- /.col -->
						</div><!-- /.mainbar-row -->
					</div><!-- /.mainbar -->
				</div><!-- /.mainbar-container -->
			</div><!-- /.mainbar-wrap -->
		</header><!-- /.main-header -->
	
	<main id="content" class="content">
		
		<!-- Home banner -->
		<section class="vc_row fullheight d-flex flex-wrap align-items-center pb-50">
			<div class="container">
				<div class="row d-flex flex-wrap align-items-center">

					<div class="lqd-column col-lg-5 col-md-6 mt-80 lqd-column" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":100,"initValues":{"translateY":51,"opacity":0},"animations":{"translateY":0,"opacity":1}}' >

						<div class="ld-fancy-heading mask-text">
							<div class="ld-fancy-heading">
								<h1 class="custom-banner-heading mb-10" data-text-rotator="true" data-split-options='{"type":"lines"}'>
									<span class="ld-fh-txt">
										<span class="txt-rotate-keywords">
											<span class="keyword active text-tertiary">Sales-Mantra</span>
											<span class="keyword text-tertiary">Business–Edge Coaching</span>
											<span class="keyword text-tertiary">Boot-Camp for Professionals</span>
										</span>
									</span>
								</h1>
								<p data-text-rotator="true">
									<span class="ld-fh-txt">
										<span class="txt-rotate-keywords">
											<span class="keyword sub-heading active text-tertiary">"your personal Guide through coaching"</span>
											<span class="keyword sub-heading text-tertiary">"your business vision guru"</span>
											<span class="keyword sub-heading text-tertiary">"7 day accelerator program for extra-ordinary career growth"</span>
										</span>
									</span>
								</p>
							</div>
						</div><!-- /.ld-fancy-heading -->

						<div class="ld-fancy-heading mask-text">
							<p
								data-split-text="true"
								data-custom-animations="true"
								data-ca-options='{"triggerHandler":"inview","animationTarget":".split-inner","duration":1200,"delay":100,"easing":"easeOutQuint","direction":"forward","initValues":{"scale":1},"animations":{"scale":1}}'
								data-split-options='{"type":"lines"}'>
								<span class="ld-fh-txt"></span>
							</p>
						</div><!-- /.ld-fancy-heading -->
						
						<div class="row">

							<div class="lqd-column col-sm-6 mb-3 mb-md-0">

								<a href="#" class="btn btn-solid text-uppercase circle btn-bordered border-thin font-size-14 font-weight-semibold" data-localscroll="true" data-localscroll-options='{"scrollBelowSection":true}'>
									<span>
										<span class="btn-txt">Explore More</span>
									</span>
								</a>

							</div>
						</div>

					</div><!-- /.col-lg-5 col-md-6 -->

					<div class="lqd-column col-lg-7 col-md-6 hidden-xs hidden-sm hidden-md">
						
						<div class="ld-masked-image" data-dynamic-shape="true">
							<svg version="1.1" class="scene" width="0" height="0" preserveAspectRatio="none" viewBox="0 0 1440 800">
								<defs>
									<clipPath id="masked-image-1">
										<path
											fill="black"
											d="M131,40 C84.135,83.534 96.819,148.446 63.283,217.394 C31.508,282.723 -3.621,324.812 1.461,394.323 C3.451,421.533 12.117,449.828 29.796,480.002 C87.412,578.34 -15.301,663.448 94.611,833.387 C156.302,928.77 316.559,918.015 435.971,936.052 C572.741,956.711 653.384,1003.601 753.566,971.715 C877.689,932.209 924.99262,809.932822 972.63862,707.700822 C1063.84662,512.000822 1038.71071,197.732895 884.476705,67.2268952 C788.919705,-13.6291048 714.704,70.008 529,43 C339.693,15.468 212.609,-35.808 131,40 Z"
											pathdata:id="
											M175.270836,26.7977911 C128.405836,70.3317911 129.938279,144.739124 96.4022789,213.687124 C64.6272789,279.016124 41.242383,286.071679 46.324383,355.582679 C48.314383,382.792679 79.5246278,459.251586 88.7738696,492.334164 C116.497714,591.496483 -75.3047466,680.552915 34.6072534,850.491915 C96.2982534,945.874915 281.559,906.015 400.971,924.052 C537.741,944.711 678.161685,902.348368 778.343685,870.462368 C902.466685,830.956368 927.354,806.232 975,704 C1066.208,508.3 1058.68971,185.848951 904.455709,55.3429506 C808.898709,-25.5130494 786.027661,117.60054 600.323661,90.5925401 C411.016661,63.0605401 256.879836,-49.0102089 175.270836,26.7977911 Z;
											M200.391256,6 C138.06059,22.7990703 77.9622177,42.6445401 44.4262177,111.59254 C12.6512177,176.92154 -4.1051307,212.01786 0.976869296,281.52886 C2.9668693,308.73886 99.0297526,534.545109 108.278994,567.627688 C136.002839,666.790006 -29.1381304,721.523368 80.7738696,891.462368 C142.46487,986.845368 331.636556,840.153183 451.048556,858.190183 C587.818556,878.849183 705.371102,948.496676 805.553102,916.610676 C929.676102,877.104676 941.497784,689.3436 989.143784,587.1116 C1080.35178,391.4116 1050.68971,206.848951 896.455709,76.3429506 C800.898709,-4.5130494 778.027661,138.60054 592.323661,111.59254 C403.016661,84.0605401 312.765712,-24.2866392 200.391256,6 Z"
										/>
									</clipPath>
								</defs>
							</svg>
							<figure class="clip-svg" data-responsive-bg="true" style="clip-path: url(#masked-image-1); -webkit-clip-path: url(#masked-image-1);">
								<img src="<?=base_url();?>assets/img/banner.jpg" alt="Image">
							</figure>
						</div><!-- /ld-masked-image -->

					</div><!-- /.col-lg-7 col-md-6 -->

				</div><!-- /.row -->
			</div><!-- /.container -->	
		</section>

		<!-- About Us -->
		<section class="vc_row pt-80 pb-50" id="about">
			<div class="container">
				<div class="row pb-20">

					<div class="lqd-column col-lg-7">
						<header class="fancy-title" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":100,"easing":"easeOutQuint","direction":"forward","initValues":{"translateX":-32,"opacity":0},"animations":{"translateX":0,"opacity":1}}'>

							<h6 class="text-uppercase ltr-sp-2 font-size-12 font-weight-semibold text-secondary">Company</h6>
							<h2 class="mt-md-4 pr-md-7 mb-4">About BridgeRift</h2>
	
							<div class="row">
								<div class="lqd-column col-sm-10 col-xs-12 text-justify">
									<?=$site->about;?>
								</div><!-- /.col-sm-10 -->
							
							</div><!-- /.row -->

						</header><!-- /.fancy-title -->

					</div><!-- /.col-lg-6 -->

					<div class="lqd-column col-lg-4 col-md-4 col-md-offset-1">
						<div class="lqd-iconbox-stack" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":"1200","startDelay":"400","delay":"100","easing":"easeOutQuint","direction":"forward","initValues":{"scaleX":0.25,"scaleY":0.25,"opacity":0},"animations":{"scaleX":1,"scaleY":1,"opacity":1}}'>
							<img src="<?=base_url();?>assets/img/profile.jpg" class="img-responsive">
						</div><!-- /.lqd-iconbox-stack -->

					</div><!-- /.col-lg-5 col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.vc_row -->

		<!-- Company Stats -->
		<section class="vc_row pt-100 pb-70 bg-cover" style="background-image: url(<?=base_url();?>assets/img/stats-bg.jpg);">
			<div class="liquid-row-overlay" style="background:rgba(9, 17, 35, 0.645)"></div>
			<div class="container">
				<div class="row">

					<div class="lqd-column col-md-3 col-sm-6 text-center">

						<div class="iconbox iconbox-xl mb-4" data-plugin-options='{"color": "#fff"}'>
							<div class="iconbox-icon-wrap">
								<span class="iconbox-icon-container mb-0 text-white">
									<i class="fa fa-users"></i>
								</span>
							</div><!-- /.iconbox-icon-wrap -->
						</div><!-- /.iconbbox -->

						<div class="liquid-counter liquid-counter-default text-white text-center">
							<div class="liquid-counter-element mb-2" data-fittext="true" data-fittext-options='{ "compressor": 0.5, "minFontSize": 55, "maxFontSize": 55 }' data-enable-counter="true" data-counter-options='{"targetNumber":"<?=$site->trained;?>+","blurEffect":true}'>
								<span><?=$site->trained;?>+</span>
							</div><!-- /.liquid-counter-element -->
							<span class="liquid-counter-text liquid-text-bottom opacity-06">Professional Trained </span>
						</div><!-- /.liquid-counter -->

					</div><!-- /.col-md-3 col-sm-6 -->

					<div class="lqd-column col-md-3 col-sm-6 text-center">

						<div class="iconbox iconbox-xl mb-4" data-plugin-options='{"color": "#fff"}'>
							<div class="iconbox-icon-wrap">
								<span class="iconbox-icon-container mb-0 text-white">
									<i class="icon-ion-ios-pin"></i>
								</span>
							</div><!-- /.iconbox-icon-wrap -->
						</div><!-- /.iconbbox -->

						<div class="liquid-counter liquid-counter-default text-white text-center">
							<div class="liquid-counter-element mb-2" data-fittext="true" data-fittext-options='{ "compressor": 0.5, "minFontSize": 55, "maxFontSize": 55 }' data-enable-counter="true" data-counter-options='{"targetNumber":"<?=$site->fortune;?>+","blurEffect":true}'>
								<span><?=$site->fortune;?>+</span>
							</div><!-- /.liquid-counter-element -->
							<span class="liquid-counter-text liquid-text-bottom opacity-06">Fortune</span>
						</div><!-- /.liquid-counter -->

					</div><!-- /.col-md-3 col-sm-6 -->

					<div class="lqd-column col-md-3 col-sm-6 text-center">

						<div class="iconbox iconbox-xl mb-4" data-plugin-options='{"color": "#fff"}'>
							<div class="iconbox-icon-wrap">
								<span class="iconbox-icon-container mb-0 text-white">
									<i class="fa fa-building"></i>
								</span>
							</div><!-- /.iconbox-icon-wrap -->
						</div><!-- /.iconbbox -->

						<div class="liquid-counter liquid-counter-default text-white text-center">
							<div class="liquid-counter-element mb-2" data-fittext="true" data-fittext-options='{ "compressor": 0.5, "minFontSize": 55, "maxFontSize": 55 }' data-enable-counter="true" data-counter-options='{"targetNumber":"<?=$site->company;?>+","blurEffect":true}'>
								<span><?=$site->company;?>+</span>
							</div><!-- /.liquid-counter-element -->
							<span class="liquid-counter-text liquid-text-bottom opacity-06">Companies</span>
						</div><!-- /.liquid-counter -->

					</div><!-- /.col-md-3 col-sm-6 -->

					<div class="lqd-column col-md-3 col-sm-6 text-center">

						<div class="iconbox iconbox-xl mb-4" data-plugin-options='{"color": "#fff"}'>
							<div class="iconbox-icon-wrap">
								<span class="iconbox-icon-container mb-0 text-white">
									<i class="fa fa-file"></i>
								</span>
							</div><!-- /.iconbox-icon-wrap -->
						</div><!-- /.iconbbox -->

						<div class="liquid-counter liquid-counter-default text-white text-center">
							<div class="liquid-counter-element mb-2" data-fittext="true" data-fittext-options='{ "compressor": 0.5, "minFontSize": 55, "maxFontSize": 55 }' data-enable-counter="true" data-counter-options='{"targetNumber":"<?=$site->certificate;?>+","blurEffect":true}'>
								<span><?=$site->certificate;?>+</span>
							</div><!-- /.liquid-counter-element -->
							<span class="liquid-counter-text liquid-text-bottom opacity-06">Certification on Business Modules</span>
						</div><!-- /.liquid-counter -->

					</div><!-- /.col-md-3 col-sm-6 -->

				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		
		<!-- Gallery -->
		<section class="vc_row pt-80 pb-120" id="gallery">
			<div class="container">
				<div class="row">

					<div class="lqd-column col-md-12">

						<div class="liquid-portfolio-list">

								<div class="row liquid-portfolio-list-row" data-columns="3"
									data-liquid-masonry="true" data-masonry-options='{ "layoutMode": "masonry", "alignMid": true }' data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":".ld-pf-item","animateTargetsWhenVisible":"true","duration":"1400","delay":"180","easing":"easeOutQuint","initValues":{"translateY":75,"scale":0.75,"opacity":0},"animations":{"translateY":0,"scale":1,"opacity":1}}'>
									
									<div class="lqd-column col-md-4 col-sm-6 col-xs-12 grid-stamp creative-masonry-grid-stamp"></div>
									
									<?php foreach($gallery as $val){?>
									<div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
										
										<div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
											
											<div class="ld-pf-inner">
												
												<div class="ld-pf-image">
													<figure style="background-image: url('<?=base_url();?>upload/<?=$val->img;?>');">
														<img src="<?=base_url();?>upload/<?=$val->img;?>" alt="Portfolio Image">
													</figure>
												</div><!-- /.ld-pf-image -->
							
												<div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
												
												<div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
													<div class="ld-pf-details-inner">
														<h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'><?=$val->name;?></h3>
													</div><!-- /.ld-pf-details-inner -->
												</div><!-- /.ld-pf-details -->
												
												<a href="<?=$val->link;?>" target="_blank" class="liquid-overlay-link"></a>
												
											</div><!-- /.ld-pf-inner -->
											
										</div><!-- /.ld-pf-item -->
									</div>
									<?php }?>

								</div><!-- /.row liquid-portfolio-list-row -->
						</div><!-- /.liquid-portfolio-list -->

					</div><!-- /.col-md-12 -->

				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>

		<!-- Services 1 -->
		<section class="vc_row pt-50 pb-90" id="services">
			<div class="container">
				<div class="row d-flex flex-wrap align-items-center">

					<div class="lqd-column col-lg-5 col-md-6 mb-5 mb-md-0" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":160,"initValues":{"translateY":50,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

						<header class="fancy-title mb-60">

							<h6 class="text-uppercase ltr-sp-2 font-size-12 font-weight-semibold text-secondary">Service</h6>
							<h2 class="mt-4 mb-4">Sales-Mantra</h2>
							<?=$service[0]->about?>
						</header><!-- /.fancy-title -->

						<a href="<?=base_url();?>home/service/sales-mantra" class="btn btn-solid text-uppercase circle btn-bordered border-thin font-size-14 font-weight-semibold" target="_blank">
							<span>
								<span class="btn-txt">Read More</span>
							</span>
						</a>
						
					</div><!-- /.col-lg-5 col-md-6 -->

					<div class="lqd-column col-lg-6 col-md-5 col-md-offset-1">
						<div class="liquid-img-group-container stretch-to-right">
							<div class="liquid-img-group-inner">
								<div class="liquid-img-group-single liquid-img-group-browser" data-shadow-style="4" data-inview="true" data-animate-shadow="true" data-reveal="true" data-reveal-options='{"direction":"lr"}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid-left">
											<a href="<?=$service[0]->video_url;?>" class="btn btn-naked text-uppercase btn-gradient fresco btn-icon-block btn-icon-top btn-icon-xlg btn-icon-circle btn-icon-solid font-size-13 text-dark btn-icon-ripple">
												<span>
													<span class="btn-icon bg-primary font-size-24"><i class="fa fa-play"></i></span>
												</span>
											</a>
										</div><!-- /.liquid-img-group-content -->
										<div class="liquid-img-container-inner">
											<figure data-responsive-bg="true">
												<?php $img = 'admin-asset/images/placeholder.jpg'; 
													if ($service[0]->img) {
														# code...
														$img = 'upload/'.$service[0]->img;
													}
												?>
												<img src="<?=base_url().$img;?>" alt="Ave" />
											</figure>
										</div><!-- /.liquid-img-container-inner -->
									</div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single -->
							</div><!-- /.liquid-img-group-inner -->
						</div>
					</div><!-- /.col-lg-6 col-md-5 col-md-offset-1 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>

		<!-- Service 2 -->
		<section class="vc_row pt-90 pb-80">
			<div class="container">
				<div class="row d-flex flex-wrap align-items-center">

					<div class="lqd-column col-md-5 hidden-xs hidden-sm">
						<div class="liquid-img-group-container stretch-to-left">
							<div class="liquid-img-group-inner">
								<div class="liquid-img-group-single liquid-img-group-browser" data-shadow-style="4" data-inview="true" data-animate-shadow="true" data-reveal="true" data-reveal-options='{"direction":"rl"}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid-right">
											<a href="<?=$service[1]->video_url;?>" class="btn btn-naked text-uppercase btn-gradient fresco btn-icon-block btn-icon-top btn-icon-xlg btn-icon-circle btn-icon-solid font-size-13 text-dark btn-icon-ripple">
												<span>
													<span class="btn-icon bg-primary font-size-24"><i class="fa fa-play"></i></span>
												</span>
											</a>
										</div><!-- /.liquid-img-group-content -->
										<div class="liquid-img-container-inner">
											<figure data-responsive-bg="true">
												<?php $img = 'admin-asset/images/placeholder.jpg'; 
													if ($service[1]->img) {
														# code...
														$img = 'upload/'.$service[1]->img;
													}
												?>
												<img src="<?=base_url().$img;?>" alt="Ave" />
											</figure>
										</div><!-- /.liquid-img-container-inner -->
									</div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single -->
							</div><!-- /.liquid-img-group-inner -->
						</div>
					</div><!-- /.col-md-5 -->

					<div class="lqd-column col-md-6 col-md-offset-1"  data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":160,"initValues":{"translateY":50,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

						<header class="fancy-title mb-60">

							<h6 class="text-uppercase ltr-sp-2 font-size-12 font-weight-semibold text-secondary">Service</h6>
							<h2 class="mt-4 mb-4">Business-Edge Coaching</h2>
							<?=$service[1]->about;?>

						</header><!-- /.fancy-title -->

						<a href="<?=base_url();?>home/service/business-edge-coaching" class="btn btn-solid text-uppercase circle btn-bordered border-thin font-size-14 font-weight-semibold" target="_blank">
							<span>
								<span class="btn-txt">Read More</span>
							</span>
						</a>

					</div><!-- /.col-md-7 -->

				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>

		<!-- Service 3 -->
		<section class="vc_row pt-100 pb-90">
			<div class="container">
				<div class="row d-flex flex-wrap align-items-center">

					<div class="lqd-column col-lg-5 col-md-6 mb-5 mb-md-0" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":160,"initValues":{"translateY":50,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

						<header class="fancy-title mb-60">

							<h6 class="text-uppercase ltr-sp-2 font-size-12 font-weight-semibold text-secondary">Service</h6>
							<h2 class="mt-4 mb-4">Boot-Camp for Professionals</h2>
							<?=$service[2]->about;?>

						</header><!-- /.fancy-title -->

						<a href="<?=base_url();?>home/service/boot-camp-for-professionals" class="btn btn-solid text-uppercase circle btn-bordered border-thin font-size-14 font-weight-semibold" target="_blank">
							<span>
								<span class="btn-txt">Read More</span>
							</span>
						</a>
						
					</div><!-- /.col-lg-5 col-md-6 -->

					<div class="lqd-column col-lg-6 col-md-5 col-md-offset-1">
						<div class="liquid-img-group-container stretch-to-right">
							<div class="liquid-img-group-inner">
								<div class="liquid-img-group-single liquid-img-group-browser" data-shadow-style="4" data-inview="true" data-animate-shadow="true" data-reveal="true" data-reveal-options='{"direction":"lr"}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid-left">
											<a href="<?=$service[2]->video_url;?>" class="btn btn-naked text-uppercase btn-gradient fresco btn-icon-block btn-icon-top btn-icon-xlg btn-icon-circle btn-icon-solid font-size-13 text-dark btn-icon-ripple">
												<span>
													<span class="btn-icon bg-primary font-size-24"><i class="fa fa-play"></i></span>
												</span>
											</a>
										</div><!-- /.liquid-img-group-content -->
										<div class="liquid-img-container-inner">
											<figure data-responsive-bg="true">
												<?php $img = 'admin-asset/images/placeholder.jpg'; 
													if ($service[2]->img) {
														# code...
														$img = 'upload/'.$service[2]->img;
													}
												?>
												<img src="<?=base_url().$img;?>" alt="Ave" />
											</figure>
										</div><!-- /.liquid-img-container-inner -->
									</div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single -->
							</div><!-- /.liquid-img-group-inner -->
						</div>
					</div><!-- /.col-lg-6 col-md-5 col-md-offset-1 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>

		<!-- client logos -->
		<section class="vc_row pt-80 pb-80 bg-light" id="clients">
			<div class="container">
				<div class="row">

					<div class="lqd-column col-md-12">
						<h2 class="mt-4 mb-5 text-center lqd-unit-animation-done">Few of Our Clients</h2>
						<div class="carousel-container carousel-nav-left carousel-nav-md carousel-dots-style1">

							<div class="carousel-items row" data-lqd-flickity='{"cellAlign":"left","prevNextButtons":false,"buttonsAppendTo":"self","pageDots":false,"groupCells":false,"wrapAround":true,"autoPlay":3000,"pauseAutoPlayOnHover":false}'>

								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-1.png" class="mt-10 w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-2.png" class="mt-10 w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-3.png" class="mt-10 w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-4.png" class="w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-5.png" class="w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-6.png" class="mt-10 w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-4.png" class="w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-5.png" class="w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->
								<div class="lqd-column carousel-item col-md-2 col-sm-3 col-xs-4">
									<figure class="text-center opacity-02 reset-opacity-onhover">
										<img src="<?=base_url();?>assets/clients/client-6.png" class="mt-10 w-60" alt="Client">
									</figure>
								</div><!-- /.carousel-item -->

							</div><!-- /.carousel-items row -->

						</div><!-- /.carousel-container -->

					</div><!-- /.col-md-12 -->

				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
	
		<!-- Testimonials -->
		<section class="vc_row pt-200 pb-100" id="testimonials">
			<div class="container">
				<div class="row">

					<div class="lqd-column col-lg-4 col-md-12" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":160,"initValues":{"translateY":50,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

						<header class="fancy-title mb-130">

							<h6 class="text-uppercase ltr-sp-2 font-size-12 font-weight-semibold text-secondary">TESTIMONIALS</h6>
							<h2 class="mt-4 mb-4">What they said</h2>
							<p>We’re humbled to be working with such a great variety of clients that range from early stage startups to Fortune 500 companies.</p>

						</header><!-- /.fancy-title -->

					</div><!-- /.col-lg-4 col-md-12 -->

					<div class="lqd-column col-lg-7 col-lg-offset-1 col-xs-12 mb-7 mb-md-0">

						<div class="carousel-container carousel-vertical-3d">

							<div class="carousel-items">

								<?php foreach($testimonials as $key => $testi){?>
								<div class="carousel-item <?=($key == 0)?'is-active':'';?>">
	
									<div class="testimonial testimonial-whole-filled testimonial-whole-shadowed text-left testimonial-details-sm testimonial-avatar-sm">
										<div class="testimonial-quote">
											<blockquote>
												<p class="font-size-16 lh-185">“<?=$testi->message;?>”</p>
											</blockquote>
										</div><!-- /.testimonial-quote -->
										<div class="testimonial-details">
											<figure class="avatar ">
												<img src="<?=base_url();?>assets/img/testi.jpg" alt="Suke Tran">
											</figure>
											<div class="testimonial-info">
												<h5><?=$testi->username;?></h5>
												<h6 class="font-weight-normal"><?=$testi->designation;?></h6>
											</div><!-- /.testimonial-info -->
											<time class=""><?=date('d-M-Y', strtotime($testi->created_on));?></time>
										</div><!-- /.testimonial-details -->
									</div><!-- /.testimonial -->
								</div><!-- /.carousel-item -->
								<?php }?>
							</div><!-- /.carousel-items -->

						</div><!-- /.carousel-container -->

					</div><!-- /.col-lg-7 col-lg-offset-1 -->

				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>

		<!-- Blog -->
		<section class="vc_row pt-100 pb-70" id="blog">
			<div class="container">
				<div class="row">

					<div class="lqd-column col-md-12">

						<div class="liquid-blog-posts">

							<div class="liquid-blog-grid row">

								<div class="lqd-column col-md-4">
									<article class="liquid-lp">
										<figure class="liquid-lp-media rounded">
											<a href="#">
												<img src="<?=base_url();?>assets/img/blog.jpg" alt="Blog Post">
											</a>
										</figure>
										<header class="liquid-lp-header">
											<h2 class="entry-title liquid-lp-title h5">
												<a href="#" rel="bookmark">Blog 1</a>
											</h2>
											<time class="liquid-lp-date font-style-italic size-lg" datetime="2019-08-16T11:33:03+00:00">5 months ago</time>
										</header>
									</article>
								</div><!-- /.col-md-4 -->

								<div class="lqd-column col-md-4">
									<article class="liquid-lp">
										<figure class="liquid-lp-media rounded">
											<a href="#">
												<img src="<?=base_url();?>assets/img/blog.jpg" alt="Blog Post">
											</a>
										</figure>
										<header class="liquid-lp-header">
											<h2 class="entry-title liquid-lp-title h5">
												<a href="#" rel="bookmark">Blog 2</a>
											</h2>
											<time class="liquid-lp-date font-style-italic size-lg" datetime="2019-08-16T11:32:46+00:00">5 months ago</time>
										</header>
									</article>
								</div><!-- /.col-md-4 -->

								<div class="lqd-column col-md-4">
									<article class="liquid-lp category-news category-rounded">
										<figure class="liquid-lp-media rounded">
											<a href="#">
												<img src="<?=base_url();?>assets/img/blog.jpg" alt="Blog Post">
											</a>
										</figure>
										<header class="liquid-lp-header">
											<h2 class="entry-title liquid-lp-title h5">
												<a href="#" rel="bookmark">Blog 3</a>
											</h2>
											<time class="liquid-lp-date font-style-italic size-lg" datetime="2019-08-16T11:32:32+00:00">5 months ago</time>
										</header>
									</article>
								</div><!-- /.col-md-4 -->
								
							</div><!-- /.liquid-blog-grid -->

						</div><!-- /.liquid-blog-posts -->

					</div><!-- /.col-md-12 -->

				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>

	</main><!-- /#content.content -->
	
	<footer class="main-footer">
		<section class="bt-fade-white-015 pt-60 pb-20" style="background: #2d2d2d; color: #e0e0e0;">
			<div class="container">
				<div class="row">
					<div class="lqd-column col-md-4">
						<figure class="mb-20">
							<img src="<?=base_url();?>assets/img/logo-white.png" alt="Logo" class="img-responsive custom-logo img-inline">
						</figure>
						<p class="text-justify">
							<?=truncate($site->about, 400);?>
						</p>
						<p>&copy; 2020 <a href="#">BridgeRift</a>. All Rights Reserved.</p>
					</div>
					<div class="lqd-column col-md-3 col-md-offset-1">
						<h6 class="font-size-22 text-white font-weight-semibold mt-0">Useful Links</h6>
						<ul class="lqd-custom-menu reset-ul text-white lh-2">
							<li><a href="#">Home</a></li>
							<li><a href="#">Sales-Mantra</a></li>
							<li><a href="#">Business Coaching</a></li>
							<li><a href="#">Boot-Camp</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</div>
					<div class="lqd-column col-md-4">
						<h6 class="font-size-22 text-white font-weight-semibold mt-0">Contact Details</h6>
						<p class="mb-0">
							<i class="fa fa-map-marker contact-icons" style="padding-left: 1px;"></i>
							<?=$site->address;?>
						</p>
						<p class="mb-0">
							<i class="fa fa-envelope-open-o contact-icons"></i> <?=$site->email;?>
						</p>
						<p>
							<i class="fa fa-phone contact-icons" style="padding-left: 1px;"></i> +91 <?=$site->contact;?>
						</p>
						<ul class="social-icon social-icon-md mb-30">
							<li><a href="<?=$site->facebook_url;?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a href="<?=$site->twitter_url;?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a href="<?=$site->youtube_url;?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
							<li><a href="<?=$site->linked_url;?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div><!-- /.col-md-6 -->
	
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>	
	</footer><!-- /.main-footer -->
	
</div><!-- /#wrap -->

<script type="text/javascript">
	var pageId = 0;
</script>
<script src="<?=base_url();?>assets/vendors/jquery.min.js"></script>
<script src="<?=base_url();?>assets/js/theme-vendors.js"></script>
<script src="<?=base_url();?>assets/js/theme.js"></script>

</body>
</html>