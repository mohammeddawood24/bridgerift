<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="theme-color" content="#3ed2a7">
	<meta name="author" description="Mohammed Dawood">
	<meta name="keywords" description="Sales-Mantra, sales mantra">

	<link rel="shortcut icon" href="./favicon.png" />
	
	<title><?=$service->service_name;?> | BridgeRift</title>
	
	<link rel="stylesheet" href="<?=base_url();?>assets/vendors/liquid-icon/liquid-icon.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/theme-vendors.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/theme.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/themes/creative.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css" />
	
	<!-- Head Libs -->
	<script async src="<?=base_url();?>assets/vendors/modernizr.min.js"></script>
	<script type="text/javascript">
		var base_url = '<?=base_url();?>';
	</script>
</head>
<body data-mobile-nav-trigger-alignment="right" data-mobile-nav-align="left" data-mobile-nav-style="modern" data-mobile-nav-shceme="gray" data-mobile-header-scheme="gray" data-mobile-nav-breakpoint="1199">
	
	<div id="wrap">
		
		<div class="titlebar titlebar-md scheme-light text-center bg-center" style="background-image: url(<?=base_url();?>assets/img/service-banner.jpg);">

			<header class="main-header main-header-overlay" data-sticky-header="true" data-sticky-options='{ "stickyTrigger": "first-section" }'>
			
				<div class="mainbar-wrap">
					<div class="megamenu-hover-bg"></div><!-- /.megamenu-hover-bg -->
					<div class="container-fluid mainbar-container">
						<div class="mainbar">
							<div class="row mainbar-row align-items-lg-stretch px-4">
								<div class="col-auto pr-5">
									<div class="navbar-header">
										<a class="navbar-brand" href="<?=base_url();?>" rel="home">
											<span class="navbar-brand-inner">
												<img class="logo-dark custom-logo" src="<?=base_url();?>assets/img/logo.png" alt="BridgeRift Sales Consultant">
												<img class="logo-default custom-logo" src="<?=base_url();?>assets/img/logo-white.png" alt="BridgeRift Sales Consultant">
											</span>
										</a>
										<button type="button" class="navbar-toggle collapsed nav-trigger style-mobile" data-toggle="collapse" data-target="#main-header-collapse" aria-expanded="false" data-changeclassnames='{ "html": "mobile-nav-activated overflow-hidden" }'>
											<span class="sr-only">Toggle navigation</span>
											<span class="bars">
												<span class="bar"></span>
												<span class="bar"></span>
												<span class="bar"></span>
											</span>
										</button>
									</div><!-- /.navbar-header -->
								</div><!-- /.col -->
								
								<div class="col">
									<div class="collapse navbar-collapse" id="main-header-collapse">
										<ul id="primary-nav" class="main-nav nav align-items-lg-stretch justify-content-lg-start" data-submenu-options='{ "toggleType":"fade", "handler":"mouse-in-out" }' data-localscroll="true">
											<li>
												<a href="#features">
													<span class="link-icon"></span>
													<span class="link-txt">
														<span class="link-ext"></span>
														<span class="txt">About</span>
													</span>
												</a>
											</li>
											<li>
												<a href="#faq">
													<span class="link-icon"></span>
													<span class="link-txt">
														<span class="link-ext"></span>
														<span class="txt">FAQ</span>
													</span>
												</a>
											</li>
											<li>
												<a href="#testimonials">
													<span class="link-icon"></span>
													<span class="link-txt">
														<span class="link-ext"></span>
														<span class="txt">Testimonials</span>
													</span>
												</a>
											</li>
											<li>
												<a href="#pricing">
													<span class="link-icon"></span>
													<span class="link-txt">
														<span class="link-ext"></span>
														<span class="txt">Pricing</span>
													</span>
												</a>
											</li>
										</ul><!-- /#primary-nav  -->
									</div><!-- /#main-header-collapse -->
								</div><!-- /.col -->
							</div><!-- /.mainbar-row -->
						</div><!-- /.mainbar -->
					</div><!-- /.mainbar-container -->
				</div><!-- /.mainbar-wrap -->
			</header><!-- /.main-header -->

			<div class="titlebar-inner">
				<div class="container titlebar-container">
					<div class="row titlebar-container">
						<div class="titlebar-col col-md-8 col-md-offset-2">
							<h1 data-fittext="true" data-fittext-options='{ "maxFontSize": 50, "minFontSize": 32 }'><?=$service->service_name;?></h1>
							<a class="titlebar-scroll-link" href="#content" data-localscroll="true"><i class="fa fa-angle-down"></i></a>
						</div><!-- /.titlebar-col -->
					</div><!-- /.titlebar-row -->
				</div><!-- /.titlebar-container -->
			</div><!-- /.titlebar-inner -->
		</div><!-- /.titlebar -->
		
		<main id="service-content" class="content">
			<!-- Features -->
			<section class="vc_row pt-60 pb-50" id="features">
				<div class="container mt-30">
					<div class="row">
						<div class="lqd-column col-md-7">
							<div class="custom-border p-3">
								<h5 class="mt-0 pr-md-7 mb-3 font-size-18 service-heading">About</h5>
								<?=$service->about;?>
								<hr>
								<h5 class="mt-0 pr-md-7 mb-3 font-size-18 service-heading">Skills that you gain</h5>
								<?php foreach(explode(',', $service->skills) as $key => $val){?>
									<span class="custom-badge"><?=ucwords($val);?></span>
								<?php }?>
							</div>
						</div>
						<div class="col-md-5">
							<div class="liquid-img-group-container mb-md-0">
								<div class="liquid-img-group-inner">
									<div class="liquid-img-group-single is-in-view" data-shadow-style="4" data-roundness="4" data-inview="true" data-animate-shadow="true">
										<div class="liquid-img-group-img-container">
											<div class="liquid-img-group-content content-floated-mid">
												<a href="<?=$service->video_url;?>" class="btn btn-naked fresco btn-icon-block btn-icon-top btn-icon-lg btn-icon-circle btn-icon-solid btn-icon-ripple">
													<span>
														<span class="btn-icon font-size-18 bg-white text-primary">
															<i class="fa fa-play"></i>
														</span>
													</span>
												</a>
											</div><!-- /.liquid-img-group-content -->
											<iframe class="embed-responsive-item" src="<?=$service->video_url;?>" width="500" height="300"></iframe><!-- /.liquid-img-container-inner -->
										</div><!-- /.liquid-img-group-img-container -->
									</div><!-- /.liquid-img-group-single -->
								</div><!-- /.liquid-img-group-inner -->
							</div>
						</div>
					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>

			<!-- FAQ -->
			<section class="vc_row pt-50 pb-50 bg-light" id="faq">
				<div class="container">
					<div class="row">
						<div class="lqd-column col-md-12">
							<h5 class="mt-0 pr-md-7 mb-3 font-size-18 service-heading1 text-center">Frequently Asked Questions</h5>
							<div class="accordion accordion-tall accordion-body-underlined accordion-expander-lg accordion-active-color-primary" id="accordion-2" role="tablist">
								<?php foreach($service_faq as $key => $faq){?>
								<div class="accordion-item panel pb-20 <?=($key == 0)?'active':'';?>">
									<div class="accordion-heading bg-white" role="tab" id="faq-heading-<?=$key;?>">
										<h4 class="accordion-title font-size-19">
											<a class="" data-toggle="collapse" data-parent="#accordion-2" href="#faq-<?=$key;?>" aria-expanded="true" aria-controls="faq-<?=$key;?>">
												<?=$faq->question;?>
												<span class="accordion-expander">
													<i class="icon-arrows_circle_plus"></i>
													<i class="icon-arrows_circle_minus"></i>
												</span>
											</a>
										</h4>
									</div>
									<div id="faq-<?=$key;?>" class="accordion-collapse collapse <?=($key == 0)?'in':'';?> bg-white" role="tabpanel" aria-labelledby="faq-heading-<?=$key;?>">
										<div class="accordion-content">
											<p><?=$faq->answer;?></p>
										</div><!-- /.accordion-content -->
									</div>
								</div>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Testimonials -->
			<section class="vc_row pt-200 pb-100" id="testimonials">
				<div class="container">
					<div class="row">

						<div class="lqd-column col-lg-4 col-md-12" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1200,"delay":160,"initValues":{"translateY":50,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

							<header class="fancy-title mb-130">

								<h6 class="text-uppercase ltr-sp-2 font-size-12 font-weight-semibold text-secondary">TESTIMONIALS</h6>
								<h5 class="mt-0 pr-md-7 mb-3 font-size-22 service-heading">What they said</h2>
								<p>We’re humbled to be working with such a great variety of clients that range from early stage startups to Fortune 500 companies.</p>

							</header><!-- /.fancy-title -->

						</div><!-- /.col-lg-4 col-md-12 -->

						<div class="lqd-column col-lg-7 col-lg-offset-1 col-xs-12 mb-7 mb-md-0">

							<div class="carousel-container carousel-vertical-3d">

								<div class="carousel-items">
									<?php foreach($service_testi as $key => $testi){?>
									<div class="carousel-item <?=($key == 0)?'is-active':'';?>">
		
										<div class="testimonial testimonial-whole-filled testimonial-whole-shadowed text-left testimonial-details-sm testimonial-avatar-sm">
											<div class="testimonial-quote">
												<blockquote>
													<p class="font-size-16 lh-185">“<?=$testi->testimonials;?>”</p>
												</blockquote>
											</div><!-- /.testimonial-quote -->
											<div class="testimonial-details">
												<figure class="avatar ">
													<img src="<?=base_url();?>assets/img/testi.jpg" alt="Suke Tran">
												</figure>
												<div class="testimonial-info">
													<h5><?=$testi->username;?></h5>
													<h6 class="font-weight-normal"><?=$testi->designation;?></h6>
												</div><!-- /.testimonial-info -->
												<time class=""><?=date('d-M-Y', strtotime($testi->created_on));?></time>
											</div><!-- /.testimonial-details -->
										</div><!-- /.testimonial -->
									</div><!-- /.carousel-item -->
									<?php }?>
								</div><!-- /.carousel-items -->

							</div><!-- /.carousel-container -->

						</div><!-- /.col-lg-7 col-lg-offset-1 -->

					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>

			<!-- Pricing -->
			<section id="pricing" class="vc_row pt-100 pb-110 bg-light">
				<div class="container">
					<div class="row">
						<div class="lqd-column col-md-12 table-responsive bg-white p-5">
							<h5 class="mt-0 mb-3 font-size-18 service-heading1 text-center">Pricing Model</h5>
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th></th>
										<th class="text-center pricing-heading p-5">Basic Program</th>
										<th class="text-center pricing-heading p-5">Silver Program</th>
										<th class="text-center pricing-heading p-5">Star Program</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($service_pricing as $key => $price){?>
									<tr>
										<td class="p-4"><?=$price->category;?></td>
										<td class="p-4 text-center"><?=$price->value1;?></td>
										<td class="p-4 text-center"><?=$price->value2;?></td>
										<td class="p-4 text-center"><?=$price->value3;?></td>
									</tr>
									<?php }?>
									<tr>
										<td class="p-4 text-center" colspan="4">
											<a href="#" class="href">For Pricing please contact us</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div><!-- /.container -->
			</section>
		</main><!-- /#content.content -->
		
		<footer class="main-footer">
			<section class="bt-fade-white-015 pt-60 pb-20" style="background: #2d2d2d; color: #e0e0e0;">
				<div class="container">
					<div class="row">
						<div class="lqd-column col-md-4">
							<figure class="mb-20">
								<img src="<?=base_url();?>assets/img/logo-white.png" alt="Logo" class="img-responsive custom-logo img-inline">
							</figure>
							<p class="text-justify">
								<?=$site->about;?>
							</p>
							<p>&copy; 2020 <a href="#">BridgeRift</a>. All Rights Reserved.</p>
						</div>
						<div class="lqd-column col-md-3 col-md-offset-1">
							<h6 class="font-size-22 text-white font-weight-semibold mt-0">Useful Links</h6>
							<ul class="lqd-custom-menu reset-ul text-white lh-2">
								<li><a href="#">Home</a></li>
								<li><a href="#">Sales-Mantra</a></li>
								<li><a href="#">Business Coaching</a></li>
								<li><a href="#">Boot-Camp</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</div>
						<div class="lqd-column col-md-4">
							<h6 class="font-size-22 text-white font-weight-semibold mt-0">Contact Details</h6>
							<p class="mb-0">
								<i class="fa fa-map-marker contact-icons" style="padding-left: 1px;"></i>
								<?=$site->address;?>
							</p>
							<p class="mb-0">
								<i class="fa fa-envelope-open-o contact-icons"></i> <?=$site->email;?>
							</p>
							<p>
								<i class="fa fa-phone contact-icons" style="padding-left: 1px;"></i> +91 <?=$site->contact;?>
							</p>
							<ul class="social-icon social-icon-md mb-30">
								<li><a href="<?=$site->facebook_url;?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?=$site->twitter_url;?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?=$site->youtube_url;?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
								<li><a href="<?=$site->linked_url;?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div><!-- /.col-md-6 -->
		
					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>	
		</footer><!-- /.main-footer -->
	</div><!-- /#wrap -->

	<script type="text/javascript">
		var pageId = 1;
	</script>
	<script src="<?=base_url();?>assets/vendors/jquery.min.js"></script>
	<script src="<?=base_url();?>assets/js/theme-vendors.js"></script>
	<script src="<?=base_url();?>assets/js/theme.js"></script>
</body>
</html>