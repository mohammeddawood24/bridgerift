<?=modules::run('common/website_header');?>
<div class="swiper-main-slider swiper-container">
	<div class="swiper-wrapper">
		<!-- Slide 1 Start -->
		<div class="swiper-slide" style="background-image:url('http://via.placeholder.com/1900x500');">
			<div class="black-overlay-60"></div>
			<div class="container">
				<div class="slider-content text-center">
					<h2 class="animated fadeInDown mb-0">
						Sales-Mantra
					</h2>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-12 offset-md-3">
							<p class="white-color animated fadeInDown">
								"Your Personal Guide Through Coaching"
							</p>
						</div>
					</div>
					<div class="animated fadeInUp mt-30">
						<a href="#" class="button-primary button-lg hvr-sweep-to-right">Learn More</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Slide 1 End -->

		<!-- Slide 2 Start -->
		<div class="swiper-slide" style="background-image:url('http://via.placeholder.com/1900x500');">
			<div class="black-overlay-70"></div>
			<div class="container">
				<div class="slider-content text-center">
					<h2 class="animated fadeInDown mb-0">
						Business–Edge Coaching
					</h2>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-12 offset-md-3">
							<p class="white-color animated fadeInDown">
								"Your Business Vision Guru"
							</p>
						</div>
					</div>
					<div class="animated fadeInUp mt-30">
						<a href="#" class="button-primary button-lg hvr-sweep-to-right">Learn More</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Slide 2 End -->
		
		<!-- Slide 3 Start -->
		<div class="swiper-slide" style="background-image:url('http://via.placeholder.com/1900x500')">
			<div class="black-overlay-70"></div>
			<div class="container">
				<div class="slider-content">
					<h2 class="text-center animated fadeInDown mb-0">
						Boot-Camp for Professionals
					</h2>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-12">
							<p class="text-center white-color animated fadeInDown">
								"7 Day Accelerator Program For Extra-Ordinary Career Growth"
							</p>
						</div>
					</div>
					<div class="text-center animated fadeInUp mt-1">
						<a href="#" class="button-white-bordered button-lg hvr-sweep-to-right">Find out more</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Slide 3 End -->
	</div>
	<!-- Add Arrows -->
	<div class="swiper-button-next"></div>
	<div class="swiper-button-prev"></div>
	<!-- Add Pagination -->
	<div class="swiper-pagination"></div>
</div>
<!-- Slider END -->

<!-- Services START -->
<div class="section-wrapper section-wrapper--gray">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-8 col-centered s-mrb-60">
				<h3 class="s-title text-center">Our Services</h3>
				<p class="s-subtitle s-subtitle--simetric">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-lg-4 text-center">
				<div class="servicesboxs">
					<img src="<?=base_url();?>upload/sales-mantra.jpg" class="mb-4" alt="services-1">
					<p class="service-link"><a href="#" class="services-title">Sales-Mantra</a></p>
					<p class="services-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
						accumsan, metus ultrices eleifend gravi, nulla nunc varius lectus, nec.</p>
					<a href="#" class="textcolor">Learn More</a>
				</div>
			</div>
			<div class="col-md-12 col-lg-4 text-center">
				<div class="servicesboxs">
					<img src="<?=base_url();?>upload/business-edge.jpg" class="mb-4" alt="services-2">
					<p class="service-link"><a href="#" class="services-title">Business-Edge Coaching</a></p>
					<p class="services-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
						accumsan, metus ultrices eleifend gravi, nulla nunc varius lectus, nec.</p>
					<a href="#" class="textcolor">Learn More</a>
				</div>
			</div>
			<div class="col-md-12 col-lg-4 text-center">
				<div class="servicesboxs">
					<img src="<?=base_url();?>upload/boot-camp.jpg" class="mb-4" alt="services-3">
					<p class="service-link"><a href="#" class="services-title">Boot-Camp for Professionals</a></p>
					<p class="services-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan, metus ultrices eleifend gravi, nulla nunc varius lectus, nec.</p>
					<a href="#" class="textcolor">Learn More</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Services END -->

<!-- Innovative START -->
<div id="innovative">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-lg-5">
				<h4 class="innovative-title textcolor mt-5">Innovative</h4>
				<h3 class="s-title">About BridgeRift</h3>
				<p class="s-subtitle text-left mt-2 mb-2">An organization with a world vision to help professionals exceed their expectations.</p>
				<p class="s-subtitle text-left mt-2 mb-2">We handhold you through every step of your career. With over 100 years of corporate experiences and in the world of training we promise to transform you into a star at the workplace.</p>
				<p class="s-subtitle text-left mt-2 mb-2">Our coaches and experts have trained 1000’s of people to make success a lifestyle. Our programs are designed to work on you to mentally motivate you stay disciplined and learn the latest techniques in the business world.</p>
				<ul class="s-mrt-10 s-mrb-30 innvlist">
					<li class="s-subtitle"><i class="fas fa-check textcolor"></i>Planning and scheduling your business.</li>
					<li class="s-subtitle"><i class="fas fa-check textcolor"></i>Doing research for your clients</li>
					<li class="s-subtitle"><i class="fas fa-check textcolor"></i>Create and manage business process</li>
				</ul>
				<a href="#" class="button-primary hvr-sweep-to-right button-md">Learn More</a>
			</div>
			<div class="col-sm-12 col-lg-6 offset-lg-1 cir-divs">
				<div class="bg-big">
					<img src="http://via.placeholder.com/80x80" alt="img-8">
				</div>
				<div class="bg-medium">
					<img src="http://via.placeholder.com/80x80" alt="img-6">
				</div>
				<div class="bg-smoll">
					<img src="http://via.placeholder.com/80x80" alt="img-7">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Innovative END -->

<!-- Portfolio START -->
<div id="portfolio" class="section-wrapper pb-0 section-wrapper--gray">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-8 col-centered">
				<h3 class="s-title text-center">Successful Webinars we've done</h3>
				<p class="s-subtitle">We work with forward-looking organizations who understand that joining the global startup economy is key to drive innovation and spur economic growth.</p>
			</div>
		</div>
	</div>

	<div class="row tabs-boxs">
		<div class="col-sm-12">
			<div class="tab-content mt-5">
				<div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-itme">
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-4 port-ite"
								 style="background-image: url('http://via.placeholder.com/635x280');">
							<div class="overlay-portfolio">
								<a href="#">Creative Startup Project</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 port-ite"
								 style="background-image: url('http://via.placeholder.com/635x280');">
							<div class="overlay-portfolio">
								<a href="#">Creative Startup Project</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 port-ite"
								 style="background-image: url('http://via.placeholder.com/635x280');">
							<div class="overlay-portfolio">
								<a href="#">Creative Startup Project</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 port-ite"
								 style="background-image: url('http://via.placeholder.com/635x280');">
							<div class="overlay-portfolio">
								<a href="#">Creative Startup Project</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Portfolio END -->

<!-- Reviews START -->
<div class="section-wrapper">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-centered">
				<h3 class="s-subtitle-top textcolor text-center">Reviews and testmonials</h3>
			</div>
			<div class="col-md-12 col-lg-10 col-centered mt-2">
				<div class="owl-reviews owl-carousel owl-theme">
					<div class="item rev-item">
						<p class="quote mt-4 mb-5">I am confident, no matter how pleased you are with your current IT, Platte River
							Networks will offer recommendations on how to improve your system making your staff more productive and
							improving your
							bottom line.</p>
						<p class="mb-5">
							<img src="http://via.placeholder.com/60x60" alt="img-15"> <span class="reviews-name">Angela Geo</span>
							<br> <span class="reviews-com">RED SEA Ltd.</span>
						</p>
					</div>
					<div class="item rev-item">
						<p class="quote mt-4 mb-5">I am confident, no matter how pleased you are with your current IT, Platte River
							Networks will offer recommendations on how to improve your system making your staff more productive and
							improving your
							bottom line.</p>
						<p class="mb-5">
							<img src="http://via.placeholder.com/60x60" alt="img-17"> <span class="reviews-name">Angela Geo</span>
							<br> <span class="reviews-com">RED SEA Ltd.</span>
						</p>
					</div>
					<div class="item rev-item">
						<p class="quote mt-4 mb-5">I am confident, no matter how pleased you are with your current IT, Platte River
							Networks will offer recommendations on how to improve your system making your staff more productive and
							improving your
							bottom line.</p>
						<p class="mb-5">
							<img src="ihttp://via.placeholder.com/60x60" alt="img-19"> <span class="reviews-name">Angela Geo</span>
							<br> <span class="reviews-com">RED SEA Ltd.</span>
						</p>
					</div>
					<div class="item rev-item">
						<p class="quote mt-4 mb-5">I am confident, no matter how pleased you are with your current IT, Platte River
							Networks will offer recommendations on how to improve your system making your staff more productive and
							improving your
							bottom line.</p>
						<p class="mb-5">
							<img src="http://via.placeholder.com/60x60" alt="img-21"> <span class="reviews-name">Angela Geo</span>
							<br> <span class="reviews-com">RED SEA Ltd.</span>
						</p>
					</div>
					<div class="item rev-item">
						<p class="quote mt-4 mb-5">I am confident, no matter how pleased you are with your current IT, Platte River
							Networks will offer recommendations on how to improve your system making your staff more productive and
							improving your
							bottom line.</p>
						<p class="mb-5">
							<img src="http://via.placeholder.com/60x60" alt="img-23"> <span class="reviews-name">Angela Geo</span>
							<br> <span class="reviews-com">RED SEA Ltd.</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Reviews END -->

<!-- Blog START -->
<div class="section-wrapper section-wrapper--gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="blog-subtitle">Latest News</p>
				<h3 class="s-title">Our insight and articles</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-4 s-mrt-40">
				<img src="http://via.placeholder.com/370x270" class="post-img" alt="img-24">
				<p class="category-title"><a href="#">Tech</a></p>
				<h3 class="post-title"><a href="#">How to start a home-based business?</a></h3>
				<p class="author">
					<img src="http://via.placeholder.com/20x20" alt="img-25"> 
					<span class="author-by">Written by
						<a href="#"><span class="textcolor author-by">Stella</span></a>
					</span> 
					<span class="date author-by">27 May, 2019</span>
				</p>
			</div>
			<div class="col-sm-12 col-md-4 s-mrt-40">
				<img src="http://via.placeholder.com/370x270" class="post-img" alt="img-26">
				<p class="category-title"><a href="#">Finance</a></p>
				<h3 class="post-title"><a href="#">How to make trust your competitive advantage</a></h3>
				<p class="author">
					<img src="http://via.placeholder.com/20x20" alt="img-29">
					<span class="author-by">Written by
						<a href="#"><span class="textcolor author-by">Stella</span></a>
					</span>
					<span class="date author-by">27 May, 2019</span>
				</p>
			</div>
			<div class="col-sm-12 col-md-4 s-mrt-40">
				<img src="http://via.placeholder.com/370x270" class="post-img" alt="img-28">
				<p class="category-title"><a href="#">Other</a></p>
				<h3 class="post-title"><a href="#">Remote workers, here’s how to dodge distractions</a></h3>
				<p class="author">
					<img src="http://via.placeholder.com/20x20" alt="img-29">
					<span class="author-by">Written by
						<a href="#"><span class="textcolor author-by">Stella</span></a>
					</span> 
					<span class="date author-by">27 May, 2019</span>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- Blog END -->
<?=modules::run('common/website_footer');?>