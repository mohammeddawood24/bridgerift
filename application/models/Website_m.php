<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/**
 *  @author <Mohammed Dawood>
 *  @Email  <mohammed.dawood24@gmail.com>
 */
class Website_m extends MY_Model{
	function __construct(){
		# code...
	}

	function get_site_details(){
		$this->db->where('id', 1);
		$query = $this->db->get('site_settings');

		return $query->row();
	}

	function service_det($slug){
		$this->db->where('slug', $slug);
		$query = $this->db->get('services');

		return $query->row();
	}

	function service_faq($service_id){
		$this->db->where('service_id', $service_id);
		$query = $this->db->get('service_faq');

		return $query->result();
	}

	function service_testi($service_id){
		$this->db->where('service_id', $service_id);
		$query = $this->db->get('service_testimonials');

		return $query->result();
	}

	function service_pricing($service_id){
		$this->db->where('service_id', $service_id);
		$query = $this->db->get('service_pricing');

		return $query->result();
	}
}