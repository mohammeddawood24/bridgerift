<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

/* ------------------------------ Date Helper ------------------------------ */
function current_date() {
   return date('Y-m-d H:i:s');
}

function get_display_date($datetimeFromMysql) {
   return date("d-m-Y H:i:s", strtotime($datetimeFromMysql));
}

function get_display_time($datetimeFromMysql) {
   return date("H:i:s", strtotime($datetimeFromMysql));
}

function current_date_only($date){
   return date("Y-m-d", strtotime($date));
}

function add_days($days, $date){
   $date = strtotime("+" . $days . " days", strtotime($date));
   return date("d/m/Y", $date);
}

function date_for_mysql($date){
   list ($d, $m, $y) = explode("-", $date);
   return $y . "-" . $m . "-" . $d;
}

function date_for_php($date){
   list ($y, $m, $d) = explode("-", $date);
   return $d . "-" . $m . "-" . $y;
}

function email_date_format($date){
   return date("d-M-Y", strtotime($date));
}

function lead_date_format($date){
   return date("M d, Y", strtotime($date));
}

function lead_time_format($time){
   return date("h:i A", strtotime($time));
}

function dashboard_date_format($date){
   return date("H:i d-M-y", strtotime($date));
}

function ticket_date_format($date){
   return date("d-M-y H:i", strtotime($date));
}

function log_email_date(){
   return date('d-m-Y H:i:s');
}

/* ------------------------------ Session	+ Site ------------------------------ */
function get_asset($type, $file, $path = 'base', $version = TRUE){
   $out = base_url() . 'asset/';
   $out .= $type . '/';
   if ($path != 'base'){
      $out .= $path . '/';
   }

   $out .= $file;

   if ($version){
      $out .= '?v=' . get_version();
   }

   return $out;
}

function get_version(){
   $CI =& get_instance();
   return $CI->config->item('version');
}

function format_for_view($str, $limit = 220, $break = " ", $camel = false, $pad = " ..."){
   $format_str = '';
   
   // return with no change if string is shorter than $limit
   if ($limit < 0 || strlen($str) <= $limit) {
      $format_str = $str;
   }else {
      $str = strip_tags($str);
      $format_str = substr($str, 0, ($limit - strlen($pad))) . $pad;
   }

   if ($camel == true) {
      return ucwords($format_str);
   }else {
      return ucfirst($format_str);
   }
}


function array_iunique($topics){
   $ltopics = array_map('strtolower', $topics);
   $cleanedTopics = array_unique($ltopics);

   foreach ($topics as $key => $value) {
      if (!isset($cleanedTopics[$key])) {
         unset($topics[$key]);
      }
   }

   return $topics;
}

function get_session_time_zone(){
   $CI = &get_instance();
   $user = $CI->session->userdata('ebridge_user');
   if (!empty($user)) {
      return $user['time_zone'];
   }else {
      return 'GMT';
   }
}

function redirect_success($path, $message){
   $CI = &get_instance();
   $CI->session->set_flashdata('success', $message);
   redirect($path);
}

function set_success(){
   $CI = &get_instance();
   $CI->session->set_flashdata('success', true);
}

function get_success(){
   $CI = &get_instance();
   $success = $CI->session->flashdata('success');
   return $success ? true : false;
}

function redirect_notice($path, $message){
   $CI = &get_instance();
   $CI->session->set_flashdata('notice', $message);
   redirect($path);
}

function redirect_error($path, $message){
   $CI = &get_instance();
   $CI->session->set_flashdata('error', $message);
   redirect($path);
}

function show_message(){
   $CI = &get_instance();
   $success = $CI->session->flashdata('success');
   $notice = $CI->session->flashdata('notice');
   $error = $CI->session->flashdata('error');

   $html = '';

   if (!empty($success))
      $html .= "<div class='alert alert-success alert-dismissible alert-gap notification' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$success</div>";
   if (!empty($notice))
      $html .= "<div class='alert alert-warning alert-dismissible alert-gap notification' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$notice</div>";
   if (!empty($error))
      $html .= "<div class='alert alert-warning alert-danger alert-gap notification' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$error</div>";

   return $html;
}

function require_user($redirect = true){
   $CI = &get_instance();
   $user = $CI->session->userdata('bridge_user');
   if (empty($user) && $redirect) {
      if ($CI->input->is_ajax_request()) {
         $CI->output->set_status_header('302');
      }else {
         $redirect = '';
         $segs = $CI->uri->segment_array();
         foreach ($segs as $segment) {
            $redirect .= '/' . $segment;
         }
         
         $CI->session->set_userdata('redirect', $redirect);
         redirect("admin/login");
      }
   }else {
      return $user;
   }
}

function initialize_admin(){
   $CI = &get_instance();
   $CI->load->library('session');
}

function str_replace_once($str_pattern, $str_replacement, $string){
   if (strpos($string, $str_pattern) !== false) {
      $occurrence = strpos($string, $str_pattern);
      return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
   }
   return $string;
}

/* ************** End of Admin Panel ****/
function get_user_time_zone(){
   $ipaddress = '';
   if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
   else if (getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
   else if (getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
   else if (getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
   else if (getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
   else if (getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
   else
      $ipaddress = 'UNKNOWN';

   if ($ipaddress == 'UNKNOWN') {
      return 'GMT';
   }else {
      $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ipaddress));
      if ($query && $query['status'] == 'success') {
         return $query['timezone'];
      }else {
         return 'GMT';
      }
   }
}

function my_send_email($recipient, $bcc_recipient = null, $sender_name, $sender_email, $reply_to, $subject, $message, $attachments = NULL, $delay = false, $alt_message = NULL){
   $CI =& get_instance();
   $config = Array(
      'protocol' => 'smtp',
      'smtp_host' => 'smtp.mailgun.org',
      'smtp_port' => 587,
      'smtp_user' => 'postmaster@ebridge.eidesign.net',
      'smtp_pass' => '6bc59ddaee1987ee6d425f2485f29ad1-6f4beb0a-b21f4f31',
      'mailtype'  => 'html', 
      'charset'   => 'utf-8'
   );
   $CI->load->library('email', $config);
   $CI->email->set_newline("\r\n");

   // Set to, from, message, etc.
   $CI->email->from('admin@eidesign.net', 'eBridge Admin');
   $CI->email->to($recipient);
   $CI->email->reply_to($reply_to, $sender_name);
   if ($bcc_recipient) {
      $CI->email->bcc($bcc_recipient);
   }

   $CI->email->subject($subject);
   $CI->email->message($message);

   if ($attachments){
      foreach ($attachments as $attachment){
         $CI->email->attach($attachment->path . $attachment->file_name);
      }
   }

   $CI->email->send();
   if ($CI->email->send())
      return true;
   else
      return false;
}

function get_random(){
   return md5(uniqid());
}

function get_base_dir(){
   $CI =& get_instance();
   return $CI->config->item('base_dir');
}

function csv_from_result($results, $titles, $delim = ',', $newline = "\n", $enclosure = '"'){
   $out = '';
   // First generate the headings from the table column names
   foreach ($titles as $title) {
      $out .= $enclosure . str_replace($enclosure, $enclosure . $enclosure, $title) . $enclosure . $delim;
   }

   $out = substr($out, 0, -strlen($delim)) . $newline;

   foreach ($results as $result) {
      $line = array();
      foreach ($result as $item) {
         $line[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $item) . $enclosure;
      }
      $out .= implode($delim, $line) . $newline;
   }
   return $out;
}

function truncate($string, $length, $dots = "...") {
   return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

function lead_status($id = NULL){
   $CI = &get_instance();
   $CI->load->model('lead_status_m');
   
   $opt = array(''=>'-None-');

   $status = $CI->lead_status_m->get_all();
   foreach ($status as $key => $value) {
      # code...
      $opt += array($value->id => $value->name);
   }

   return isset($id) ? $opt[$id] : $opt;
}

function lead_source($id = NULL){
   $CI = &get_instance();
   $CI->load->model('lead_source_m');
   
   $opt = array(''=>'-None-');

   $source = $CI->lead_source_m->get_all();
   foreach ($source as $key => $value) {
      # code...
      $opt += array($value->id => $value->name);
   }

   return isset($id) ? $opt[$id] : $opt;
}

function getUserStatus($id = NULL){
   $opt = array(''=>'--Select Status--', '0'=>'Inactive', '1'=>'Active', '2'=>'Suspended');

   return isset($id) ? $opt[$id] : $opt;
}

function getUserRoles($id = NULL){
   $opt = array(''=>'--Select Role--', '0'=>'Super Admin', '1'=>'Marketing Manager', '2'=>'User');

   return isset($id) ? $opt[$id] : $opt;
}

function activity_type($id = NULL){
   $opt = array('Task', 'Meeting');

   return isset($id) ? $opt[$id] : $opt;
}

function priority($id = NULL){
   $opt = array('High', 'Highest', 'Low', 'Lowest', 'Normal');

   return isset($id) ? $opt[$id] : $opt;
}

function get_company($company_id = NULL){
   $CI = &get_instance();
   $CI->load->model('company_m');

   $companies = $CI->company_m->get_all();
   $opt = array('--Select Company--');

   foreach ($companies as $company) {
      # code...
      $opt += array($company->id => $company->name);
   }

   return isset($company_id) ? $opt[$company_id] : $opt;
}

function get_company_contacts($company_id, $contact_id = NULL){
   $CI = &get_instance();
   $CI->load->model('company_contact_m');

   $contacts = $CI->company_contact_m->get_many_by(array('company_id'=>$company_id));
   $opt = array('--Select Contact Person--');

   foreach ($contacts as $contact) {
      # code...
      $opt += array($contact->id => $contact->name);
   }

   return isset($contact_id) ? $opt[$contact_id] : $opt;
}